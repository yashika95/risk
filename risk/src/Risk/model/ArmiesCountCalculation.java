package Risk.model;

/**
 * This Class is used to select the Number of initial
 * armies depending on Number of Players playing.
 *
 */
public class ArmiesCountCalculation {

    public int armiesCount;
    /** This function is created to assign the players by using the rule 50-(number of player*5)
	 * @param numberofPlayers: Number of Players playing the game.
	 */
    public void initialArmiesCountCalculation(int numberofPlayers) {

        armiesCount = 50 - (numberofPlayers * 5);

    }

    /**
	 * Returns the Number of initial armies per Player.
	 * @return Number of initial armies
	 */
    public int getArmiesCount() {

        return armiesCount;
    }
    /**
	 * Set the Number of initial Armies assigned per Player.
	 * @param armiesCount: Number of initial armies
	 */
    public void setArmiesCount(int armiesCount) {

        this.armiesCount = armiesCount;

    }

}
