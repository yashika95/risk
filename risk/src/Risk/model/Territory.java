package Risk.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * This class is used to handle territories
 */
public class Territory {
    public Map<String, List<String>> adjacentTerritories;
    public Map<String, Integer> TerritoryStatusMap;
    Map<String, String> territoryCard;
    static int totalTerritories=0;
/**
 * Constructor of Territory class
 */
    public Territory(){
        adjacentTerritories= new HashMap<>();
        TerritoryStatusMap= new HashMap<>();
        territoryCard=new HashMap<>();

    }
/**
 * This function is used to set adjecent Territories of the provided territories
 * @param territoriesData: information about the territories
 */
    public void setAdjacentTerritories(String territoriesData){
        for(String data:territoriesData.split("\n")){
            String[] territoriesDataArray = data.split(",");
            if(territoriesDataArray.length>1){
                System.out.println(territoriesDataArray[0]);
                List<String> list = new ArrayList<>();
                this.adjacentTerritories.put(territoriesDataArray[0],list);
                this.TerritoryStatusMap.put(territoriesDataArray[0],0);
                this.territoryCard.put(territoriesDataArray[0],"");
                for(int i=4; i<territoriesDataArray.length;i++){
                    List<String> adjacentTerritoriesList= adjacentTerritories.get(territoriesDataArray[0]);
                    adjacentTerritoriesList.add(territoriesDataArray[i]);
                }

            }
        }
        System.out.println(adjacentTerritories);
        totalTerritories=TerritoryStatusMap.size();
    }

/**
 * This function is used to get status of territory map
 * @return status of territory map
 */
    public Map getTerritoryStatusMap(){
        return TerritoryStatusMap;
    }
    /**
     * This function is used to get adjecent territories
     * @return adjecent territories
     */
    public Map getAdjacentTerritories(){
        return this.adjacentTerritories;
    }
/**
 * This method is used to get adjacent territory list
 * @param territoryName:name of territory
 * @return list of adjacent territory
 */
    public List<String> getAdjacentTerritoryList(String territoryName){
        List<String> territoryList= adjacentTerritories.get(territoryName);
        return territoryList;
    }
/**
 * This method is used to get Territory Card
 * @return Map of Territory Card
 */
    public Map getTerritoryCard(){
        return this.territoryCard;
    }
/**
 * This method is used to set Territory Card
 */
    public void setTerritoryCard() {
        ArrayList<String> cardsList=new ArrayList<>();

        for(int i=0;i<14;i++){
            cardsList.add("infantry" );
            cardsList.add("cavalry");
            cardsList.add("artillery");
        }

        for (Map.Entry<String, String> territory : territoryCard.entrySet()) {
            int size = cardsList.size();
            int randomNumber = (int) (Math.random() * size);
            String cardName = cardsList.get(randomNumber);
            territory.setValue(cardName);
            cardsList.remove(randomNumber);
        }
        System.out.println(territoryCard);
    }

}
