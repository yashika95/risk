package Risk.model;

import java.util.*;
/**
 * This is a class for continentValue, continentTerritory,
 * continentOwnedTerritory, playerOwnedContTerr as its data member.
 *
 */
public class Continent {

    public Map<String, Integer> continentName;
    public Map<String, List<String>> continentTerritories;

    public Continent(){
        continentName= new HashMap<>();
        continentTerritories= new HashMap<>();
    }
   /**
    * This function is used to set Continent Name based on Continent Data provided by the user
    * @param continentData: Data provied by the user 
    */
    public void setContinentName(String continentData)
    {
        System.out.println(continentData);
        for (String data : continentData.split("\n")){
            System.out.println(data);
                String[] continentsDataArray = data.split("=");
                if(continentsDataArray.length==2) {
                    System.out.println(continentsDataArray[1]);
                    int continentdataarray=Integer.parseInt(continentsDataArray[1].trim());
                    this.continentName.put(continentsDataArray[0], continentdataarray);
                    List<String> list = new ArrayList<String>();
                    this.continentTerritories.put(continentsDataArray[0],list);
                }
        }
        System.out.println(continentName);
    }

    /**
     * This function is used to set Continent Territories based on Continent Territories Data provided by the user
     * @param territoriesData:Territories Data provied by the user
     */
    public void setContinentTerritories(String territoriesData){
        for(String data:territoriesData.split("\n")){
            String[] territoriesDataArray = data.split(",");
           if(territoriesDataArray.length>1){
               System.out.println(territoriesDataArray[0]);
               List<String> list = continentTerritories.get(territoriesDataArray[3]);
               list.add((territoriesDataArray[0]));
           }
        }
        System.out.println(continentTerritories+"mytest");
    }
/**
 * This function is used to get Continent Name
 * @return returns Continent name
 */
    public Map getContinenttName(){
        return this.continentName;
    }
    /**
     * This function is used to get Continent Territories
     * @return returns Continent Territories
     */
    public Map getContinentTerritories(){
        return continentTerritories;
    }
}
