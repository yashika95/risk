package Risk.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * This class is used to generate armies of player in Reinforcement phase.
 * 
 */
public class Reinforcement 
{
    HashMap<String, Integer> playersReinforcementarmyCount;
/**
 * Constructor of Reinforcement Class
 */
    public Reinforcement(){
        playersReinforcementarmyCount= new HashMap<>();
    }
    /**
	 * This method allow player to perform Reinforcement Phase of Game
	 * @param playersOwnedTerritories: Provides list of player owned Territorties
	 */
    public void setPlayersReinforcementarmyCount(Map<String, ArrayList<String>> playersOwnedTerritories){
        Set<String> keys = playersOwnedTerritories.keySet();
        for(String key: keys){
            ArrayList<String> territoryList=playersOwnedTerritories.get(key);
            int reinforcementCount= (int) Math.floor(territoryList.size()/3);
            if(reinforcementCount<3){
                reinforcementCount=3;
            }
            playersReinforcementarmyCount.put(key, reinforcementCount);

        }

        System.out.println(playersReinforcementarmyCount);

    }

    /**
     * This method is used to get player's reinforced army count
     * @param playerName:name of the player
     * @return count:count of number of armies
     */

    public int getPlayerReinforcementArmyCount(String playerName){
        int count=playersReinforcementarmyCount.get(playerName);
        return count;
    }
/** 
 * This method is used to set player Reinforcement Army Count
 * @param playerName:Name of the player
 * @param count:army count
 */
    public void setPlayerReinforcementArmyCount(String playerName, int count){
        playersReinforcementarmyCount.put(playerName, count);

    }
}
