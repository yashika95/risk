package Risk.model;

import Risk.observer.DenominationSub;
import Risk.observer.ExchangeSubject;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.util.*;

import Risk.observer.Subject;
import org.apache.commons.lang3.StringUtils;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

/**
 * This class defines Player roles
 */
public class Players {

    public ArrayList<String> playersNameList;
    public HashMap<String, Integer> playerarmyCount;
    public Map<String, ArrayList<String>> playersOwnedTerritories;
    public HashMap<String, ArrayList<Integer>> assignedArmies;
    public Map<String, ArrayList<String>> playersOwnedCards;
    static int cardsArmy=0;
    static boolean endGame=false;


    public Players(){
        playersOwnedTerritories= new HashMap<>();
        assignedArmies= new HashMap<>();
        playersOwnedCards=new HashMap<>();

    }
/**
 * This function is used to add the players
 * @param name:Defines name of the player
 */
    public void addPlayers(ArrayList<String> name){

        playersNameList = name;
        System.out.println(playersNameList);

        for(int i=0; i<name.size();i++){
            ArrayList<String> list = new ArrayList<>();
            playersOwnedTerritories.put(name.get(i), list);
            playersOwnedCards.put(name.get(i),list);
        }
        System.out.println(playersOwnedTerritories);
    }
/**
 * This function define initial army count of the player
 * @param name:name of the player
 * @param playersarmyCount:army count of the player
 * @return players army count
 */
    public Map<String, Integer> initialarmyCount(String name, int playersarmyCount) {

        playerarmyCount = new HashMap<String, Integer>();

        playerarmyCount.put(name, playersarmyCount);

        return playerarmyCount;

    }
/**
 * This function is used to get initial army Count
 * @param name:name of the player
 * @return:army count of the player
 */
    public int getinitialarmyCount(String name) {

        return playerarmyCount.get(name);

    }
    /**
     * This function is used to get list of territories owned by the players
     * @return  list of territories owned by the players
     */

    public Map<String, ArrayList<String>> getPlayersOwnedTerritories(){
        return this.playersOwnedTerritories;
    }
/**
 * This function is used to get list of territories owned by the player
 * @param playerName input player name
 * @return list of territories owned by the players
 */
    public ArrayList<String> getPlayerOwnedTerritories(String playerName){
        return playersOwnedTerritories.get(playerName);
    }
    /**
     * This function is used to set list of territories owned by the player
     * @param playersOwnedTerritoriesData: list of territories owned by the players
     */
    public void setPlayersOwnedTerritories(Map<String, ArrayList<String>> playersOwnedTerritoriesData){
        this.playersOwnedTerritories= playersOwnedTerritoriesData;
    }
/**
 * This method is used to get Territory army
 * @param territoryName:name of terrritory
 * @return army:army of that particular territory
 */
    public int getTerritoryArmy(String territoryName) {

        int army = 0;
        for (int i = 0; i < playersNameList.size(); i++) {
            List<String> territoryList = playersOwnedTerritories.get(playersNameList.get(i));
            ArrayList<Integer> armiesList = assignedArmies.get(playersNameList.get(i));
            if (territoryList.contains(territoryName)) {
                int index = territoryList.indexOf(territoryName);
                army = armiesList.get(index);
                break;
            }
          }
        return army;
    }
/**
 * This method is used to get name of the player
 * @param territoryName:name of terrritory
 * @return playername:name of the player 
 */
    public String getTerritoryPlayerName(String territoryName){
        String playerName = null;
        for(int i=0; i<getPlayersNameList().size();i++){
            List<String> territoryList= playersOwnedTerritories.get(playersNameList.get(i));
            if(territoryList.contains(territoryName)){
                playerName= playersNameList.get(i);
                break;
            }
        }
        return playerName;
    }
/**
 * This method is used to get territory Index number
 * @param territoryName:name of terrritory
 * @param playerName:name of the player
 * @return index:index number
 */
    public int getTerritoryIndex(String territoryName, String playerName){
        List<String> territoryList= playersOwnedTerritories.get(playerName);
        int index=territoryList.indexOf(territoryName);
        System.out.println(index);
        return index;
    }
/**
 * This method is used to set territory to the attacker
 * @param currentPlayerName:name of current player
 * @param attackedPlayerName:name of player who attacked
 * @param attackedTerritory:name of the territory which has been attacked
 * @param index:index number of territory
 */
    public void setTerritoryToAttacker(String currentPlayerName, String attackedPlayerName, String attackedTerritory, int index){
        playersOwnedTerritories.get(attackedPlayerName).remove(index);
        assignedArmies.get(attackedPlayerName).remove(index);
        playersOwnedTerritories.get(currentPlayerName).add(attackedTerritory);
        assignedArmies.get(currentPlayerName).add(0);
        System.out.println(playersOwnedTerritories);
        System.out.println(assignedArmies);

    }
/**
 * This method is used to check wheather attack is possible or not
 * @param currentPlayerName:name of current player
 * @return true if possible else false 
 */
    public boolean checkIsAttackPossible(String currentPlayerName){
        boolean result=false;
        ArrayList<Integer> armiesList=assignedArmies.get(currentPlayerName);
        for(int i=0; i<armiesList.size();i++){
            int army=armiesList.get(i);
            if(army>1){
                result=true;
                break;


            }
        }
        return result;

    }
/**
 * This method is used to get list of card player has
 * @param currentPlayerName:name of current player
 * @return List of cards
 */
    public List getPlayerCardList(String currentPlayerName){
        return playersOwnedCards.get(currentPlayerName);
    }
/**
 * This method is used to assign card to player
 * @param currentPlayerName:name of current player
 * @param cardName:name of current player
 */
    public void setPlayerCard(String currentPlayerName, String cardName){
        List<String> cardsList=playersOwnedCards.get(currentPlayerName);
        cardsList.add(cardName);
    }


/**
 * This method is used to set army to attack
 * @param currentPlayerName:Name of current player
 * @param attackedTerritory:name of the territory which has been attacked
 * @param attackerTerritory:name of the territory from which attack is happening
 * @param moveArmy:count of army to attack
 * @return true if army can move else return false
 */
    public boolean setArmyToAttackedTerritory(String currentPlayerName, String attackedTerritory, String attackerTerritory, int moveArmy) {
        int attackerTerritoryArmy= getTerritoryArmy(attackerTerritory);
        attackerTerritoryArmy=attackerTerritoryArmy-moveArmy;
        if(attackerTerritoryArmy>=1) {
            int attackerTerritoryIndex = getTerritoryIndex(attackerTerritory, currentPlayerName);
            int attackedTerritoryIndex = getTerritoryIndex(attackedTerritory, currentPlayerName);
            setAssignedArmies(currentPlayerName, attackerTerritoryIndex, attackerTerritoryArmy);
            setAssignedArmies(currentPlayerName, attackedTerritoryIndex, moveArmy);
            return true;
        }
        else {
            System.out.println("Can't move this no. of armies");
            return false;
        }

    }
    /**
     * This function is used to assign random territories to the players
     * by selecting territory randomly and assigning them one by one to the
     * player hashmap
     * @param territoryData:contains territory data
     * @param territory:territory list
     * @param players:playersList
     */
    public void assignRandomTerritories(Map territoryData, Territory territory, Players players){
        Map<String, Integer> TerritoriesList= territoryData;
        System.out.println(TerritoriesList.size());
        Map<String, ArrayList<String>> playersOwnedTerritory= players.getPlayersOwnedTerritories();
        System.out.println("Data"+playersOwnedTerritory);
        for(Map.Entry<String, ArrayList<String>> playersHashmap: playersOwnedTerritory.entrySet()){

                      if(!TerritoriesList.isEmpty()) {
                          Object randomName = TerritoriesList.keySet().toArray()[new Random().nextInt(TerritoriesList.keySet().toArray().length)];
                          System.out.println(randomName);
                          ArrayList<String> list = new ArrayList<>();

                          list.add(String.valueOf(randomName));
                          playersOwnedTerritory.put(playersHashmap.getKey(), list);
                          TerritoriesList.remove(randomName);

                      }
              }
        while (!TerritoriesList.isEmpty()){

            for(Map.Entry<String, ArrayList<String>> playersHashmap: playersOwnedTerritory.entrySet()){

                if(!TerritoriesList.isEmpty()) {
                    Object randomName = TerritoriesList.keySet().toArray()[new Random().nextInt(TerritoriesList.keySet().toArray().length)];
                    System.out.println(randomName);
                    ArrayList<String> list = playersHashmap.getValue();

                    list.add(String.valueOf(randomName));
                    playersOwnedTerritory.put(playersHashmap.getKey(), list);
                    TerritoriesList.remove(randomName);

                }

            }
        }

       System.out.println(TerritoriesList);
       System.out.println(playersOwnedTerritory);
       this.playersOwnedTerritories=playersOwnedTerritory;

    }

/**
 * This function is used to assign initial army to the Territory
 * by the rule assigning them 1 by 1
 * @param armiesCount:Count of armies
 */
    public void assignInitialArmytoTerritory(int armiesCount){

        Set<String> keys= playersOwnedTerritories.keySet();

        for(String key:keys){

            int size=playersOwnedTerritories.get(key).size();
            ArrayList<Integer> list= new ArrayList<>(size);
            for(int i=0; i<=size-1; i++){
                list.add(1);
            }

            assignedArmies.put(key, list);
        }

        Set<String> playerNames= assignedArmies.keySet();
        int count;
        int armies;
        for(String playerName: playerNames){
            count=0;
            armies=1;
            int size=assignedArmies.get(playerName).size();
            ArrayList<Integer> list= assignedArmies.get(playerName);

            while(count<armiesCount) {
                for (int i = 0; i <=size-1; i++) {
                    if(count<armiesCount) {
                        list.set(i, armies);
                        count++;
                    }
                }
                armies=armies+1;
            }

        }


        System.out.println(assignedArmies);


    }
/**
 * This method is used to get list of names of players
 * @return players name list
 */
    public ArrayList<String> getPlayersNameList() {
        return playersNameList;
    }
/**
 * This method is used to get list of armies assigned to the player
 * @param playerName input player name
 * @return list of armies 
 */
    public ArrayList<Integer> getPlayerAssignedArmies(String playerName){
        ArrayList<Integer> armiesList= assignedArmies.get(playerName);
        return armiesList;
    }
/**
 * This method is used to set assigned armies
 * @param playerName:name of player
 * @param index:index value
 * @param value:Number of army count
 */
    public void setAssignedArmies(String playerName, int index, int value){
        ArrayList<Integer> armiesList= assignedArmies.get(playerName);
        armiesList.set(index, value);

    }
    /**
     * This method defines about player owned continents army
     * @param currentPlayerName:Name of current player
     * @param continent:name of continent
     * @param players:list of players
     * @return control value
     */
    public int playerOwnedContinentsArmy(String currentPlayerName, Continent continent, Players players) {
        int totalControlValue=0;
        for (Map.Entry<String, List<String>> key: continent.continentTerritories.entrySet()) {

                String continentName = key.getKey();

                List<String> continentTerritories = key.getValue();
                List<String> playerTerritories = players.getPlayerOwnedTerritories(currentPlayerName);

                if (playerTerritories != null) {

                    Boolean commonTerritories = playerTerritories.containsAll(continentTerritories);

                    if (commonTerritories) {

                        int controlValue=continent.continentName.get(continentName);
                        totalControlValue=totalControlValue+controlValue;
                        System.out.println("Owned continet- "+continentName+" value- "+controlValue);

                    }
                }


        }

        return totalControlValue;

    }
/**
 * This method is used to check if adjecent territories are connected or not
 * @param initialTerritory:name of initial territory
 * @param destinationTerritory:name of destination territory
 * @param playerName:name of player
 * @param territory:list of territory
 * @return true if connected else return false
 */
    public boolean checkConnectedAdjacent(String initialTerritory, String destinationTerritory, String playerName, Territory territory ){
        Map<String, List<String>> adjacentTerritories= territory.getAdjacentTerritories();
        System.out.println(adjacentTerritories);
        ArrayList<String> territoriesList= getPlayerOwnedTerritories(playerName);
        System.out.println(territoriesList);
        Map<String, List<String>> adjacentTerritory= new HashMap<>();
        Map<String, Integer> statusMap= new HashMap<>();
        for(int i=0; i<territoriesList.size();i++){
            List<String> adjacentList= new ArrayList<>();
            List<String> sublist= adjacentTerritories.get(territoriesList.get(i));
            for(int j=0; j<sublist.size(); j++){
                if(territoriesList.contains(sublist.get(j))){
                    adjacentList.add(sublist.get(j));
                }

            }
            adjacentTerritory.put(territoriesList.get(i), adjacentList);
            statusMap.put(territoriesList.get(i),0);
        }
        System.out.println(adjacentTerritory);
        System.out.println(statusMap);

        Queue<String> queue=new LinkedList<>();
        String firstTerritory=initialTerritory;
        // logic to add in queue
        statusMap.put(firstTerritory,1);
        System.out.println(statusMap);
        List<String> adjacent=adjacentTerritory.get(firstTerritory);
        System.out.println(adjacent);
        for(int i=0; i<adjacent.size(); i++){
            String adjacentData= adjacent.get(i);
            queue.add(adjacentData);
        }
        while(!queue.isEmpty()){
            String firstElement= ((LinkedList<String>) queue).pollFirst();
            System.out.println(firstElement);
            List<String> adjacentData=adjacentTerritory.get(firstElement);
            System.out.println(adjacentData);
            for(int i=0; i<adjacentData.size(); i++){
                String adjacentElements= adjacentData.get(i);
                if(!statusMap.get(adjacentElements).equals(1)) {
                    queue.add(adjacentElements);
                    System.out.println(queue);
                }
            }

            System.out.println(queue);
            statusMap.put(firstElement,1);
            System.out.println(statusMap);
        }
        if(0==statusMap.get(destinationTerritory)){
            System.out.println("disconnected map");
            System.out.println(queue);
            return false;
        }
        else{
            System.out.println(queue);
            return true;
        }

    }

    public boolean checkPlayerLost(String attackedPlayerName){
        List<String> territoryList=playersOwnedTerritories.get(attackedPlayerName);
        if(territoryList.isEmpty()){
            System.out.println("Player-"+attackedPlayerName+" lost all the territories");
            //remove name from the player list and playersowned Territories
            playersOwnedTerritories.remove(attackedPlayerName);
            playersNameList.remove(attackedPlayerName);
            assignedArmies.remove(attackedPlayerName);
            playerarmyCount.remove(attackedPlayerName);
            return true;
        }
        return false;
    }

    public void getLostPlayerCards(String currentPlayerName, String attackedPlayerName){
        List<String> currentplayerCardList=playersOwnedCards.get(currentPlayerName);
        List<String> attackedPlayerCardList=playersOwnedCards.get(attackedPlayerName);
        if(!attackedPlayerCardList.isEmpty()){
            for(int i=0; i<attackedPlayerCardList.size();i++){
                currentplayerCardList.add(attackedPlayerCardList.get(i));
            }
            playersOwnedCards.remove(attackedPlayerName);
            System.out.println(playersOwnedCards);
        }
        else{
            System.out.println("Lost player don't have any cards");
            playersOwnedCards.remove(attackedPlayerName);
        }

    }

    public boolean checkGameEnd()
    {
        boolean isGameEnd=false;
        for(int i=0;i<playersNameList.size();i++){
            List<String> territoryList=playersOwnedTerritories.get(playersNameList.get(i));
            int totalTerritories= Territory.totalTerritories;
            if(totalTerritories==territoryList.size()){
                isGameEnd=true;
                System.out.println("Player-"+playersNameList.get(i)+" won the game");

            }
        }
        return isGameEnd;
    }
/**
 * This method is used to start the game
 * @param reinforcement:object of Reinforcement class
 * @param players:object of Player class
 * @param territory:object of Territory class
 * @param continent:object of Continent class
 * @param subject:object of Subject class
 * @param denominationSub:object of DenominationSubject class
 * @param exchangeSubject:object of Exchange Subject of class
 * @return true if game start else return false 
 */

    public boolean StartGame(Reinforcement reinforcement, Players players, Territory territory, Continent continent, Subject subject, DenominationSub denominationSub, ExchangeSubject exchangeSubject){

        ArrayList<String> playersNameList= players.getPlayersNameList();
        System.out.println(playersNameList);
        boolean endgame;

        for(int i=0; i<playersNameList.size();i++)
        {   List<String> cardsList=new LinkedList<>();
            int isAttackWon=0;
            System.out.println("Players world domination view");
            players.playerWorldDominationView(players, territory, continent,denominationSub);
        	subject.setPhase("------Game Started-------");
            System.out.println("------Game Started-------");
            String currentPlayerName = playersNameList.get(i);
            //if (currentPlayerName != "Neutral Player")
           // {
            	subject.setPlayerName("Current Player Name :" + currentPlayerName);
                System.out.println("Current Player Name :" + currentPlayerName);

                players.reinforcementPhase(currentPlayerName, players, reinforcement, continent, territory, subject, exchangeSubject);
                isAttackWon=players.attackPhase(territory, currentPlayerName, players, subject);
                if(isAttackWon>0)
                {

                    System.out.println(playersOwnedCards);
                    String territoryCardKey= (String) territory.territoryCard.keySet().toArray()[0];
                    String territoryCardValue= territory.territoryCard.get(territoryCardKey);
                    String card= territoryCardKey+"-"+territoryCardValue;
                    players.setPlayerCard(currentPlayerName, card);
                    System.out.println(playersOwnedCards);
                    //remove card from the pile
                    territory.territoryCard.remove(territoryCardKey);
                    System.out.println("card won "+card+" player name "+currentPlayerName);
                }

                endgame= players.checkGameEnd();
                if(endgame==true){
                    //game ended
                    endGame=true;
                    System.out.println("Game ended");
                    return endGame;
                }

                System.out.println("Press 1, If you want to start Fortification Phase");
                System.out.println("Press 2, If you want to End Turn");
                Scanner sci = new Scanner(System.in);
                int option = sci.nextInt();
                players.fortificationPhase(option,currentPlayerName, players, reinforcement, territory, subject);


            //}
        }

        return endGame;

    }

/**
 * This function is to run reinforcement phase
 * @param currentPlayerName:name of current player
 * @param players:object of Player class
 * @param territory:object of Territory class
 * @param continent:object of Continent class
 * @param subject:object of Subject class
 * @param exchangeSubject:object of Exchange Subject of class
 * @param reinforcement:object of Reinforcement Subject class
 */
    public void reinforcementPhase(String currentPlayerName, Players players, Reinforcement reinforcement, Continent continent, Territory territory, Subject subject,ExchangeSubject exchangeSubject){
        reinforcement.setPlayersReinforcementarmyCount(playersOwnedTerritories);
        int reinforcementArmy = reinforcement.getPlayerReinforcementArmyCount(currentPlayerName);
        System.out.println("Armies by no. of Territories- "+reinforcementArmy);
        int controlValueArmy= players.playerOwnedContinentsArmy(currentPlayerName,continent,players);
        System.out.println("Armies by control value"+ controlValueArmy);
        int cardsarmies=players.cardView(currentPlayerName, players, territory,exchangeSubject);
        reinforcementArmy=reinforcementArmy+controlValueArmy;
        reinforcementArmy=reinforcementArmy+cardsarmies;
        subject.setPhase("---------Reinforcement phase started--------");
        System.out.println("---------Reinforcement phase started--------");
        while (reinforcementArmy > 0) {
            subject.setreinforcementMsg("Current reinforcement armies = " + reinforcementArmy);
            System.out.println("Current reinforcement armies = " + reinforcementArmy);
            ArrayList<String> territoriesList = players.getPlayerOwnedTerritories(currentPlayerName);
            ArrayList<Integer> armiesList = players.getPlayerAssignedArmies(currentPlayerName);
            int noOfTerritories = territoriesList.size();
            subject.setreinforcementMsg("Number of Territories Owned " + noOfTerritories);
            System.out.println("Number of Territories Owned " + noOfTerritories);
            for (int j = 0; j < noOfTerritories; j++) {
                System.out.println(territoriesList.get(j) + " - " + armiesList.get(j));
            }

            int placeArmies;
            //code for the aggressive player-reinforces its strongest Country
            if (currentPlayerName.equals("AGGRESSIVE")) {
                placeArmies = reinforcementArmy;
                String aggressiveTerritoryName=players.getPlayerHighestArmyTerritory(currentPlayerName);
                System.out.println(aggressiveTerritoryName);
                int index = territoriesList.indexOf(aggressiveTerritoryName);
                int value = armiesList.get(index);
                value = value + placeArmies;
                System.out.println("updated armies " + value);
                players.setAssignedArmies(currentPlayerName, index, value);
                reinforcementArmy = reinforcementArmy - placeArmies;
                System.out.println(playersOwnedTerritories);
                System.out.println(assignedArmies);

            }
            else if (currentPlayerName.equals("BENEVOLENT")) {
                placeArmies = reinforcementArmy;
                String benevolentTerritoryName=players.getPlayerWeakestArmyTerritory(currentPlayerName);
                System.out.println(benevolentTerritoryName);
                int index = territoriesList.indexOf(benevolentTerritoryName);
                int value = armiesList.get(index);
                value = value + placeArmies;
                System.out.println("updated armies " + value);
                players.setAssignedArmies(currentPlayerName, index, value);
                reinforcementArmy = reinforcementArmy - placeArmies;
                System.out.println(playersOwnedTerritories);
                System.out.println(assignedArmies);

            }
            else if (currentPlayerName.equals("RANDOM")) {

                placeArmies = (int )(Math.random() * reinforcementArmy + 1);
                System.out.println("RANDOM ARMIES"+placeArmies);
                String randomTerritoryName=players.getPlayerRandomArmyTerritory(currentPlayerName);
                System.out.println("RANDOM TERRITORY"+randomTerritoryName);
                int index = territoriesList.indexOf(randomTerritoryName);
                int value = armiesList.get(index);
                value = value + placeArmies;
                System.out.println("updated armies " + value);
                players.setAssignedArmies(currentPlayerName, index, value);
                reinforcementArmy = reinforcementArmy - placeArmies;
                System.out.println(playersOwnedTerritories);
                System.out.println(assignedArmies);

            }
            else if (currentPlayerName.equals("CHEATER")) {

                ArrayList<Integer> cheaterArmies=assignedArmies.get(currentPlayerName);
                for(int i=0; i<cheaterArmies.size();i++){
                    int value=cheaterArmies.get(i);
                    value=value*2;
                    assignedArmies.get(currentPlayerName).set(i,value);
                }
                reinforcementArmy=0;
                System.out.println(playersOwnedTerritories);
                System.out.println(assignedArmies);

            }
            else{

            subject.setreinforcementMsg("Enter the no. of armies you want to place");
            System.out.println("Enter the no. of armies you want to place");
            Scanner sc = new Scanner(System.in);
            placeArmies = sc.nextInt();
            System.out.println(placeArmies);

            if (placeArmies > reinforcementArmy) {
                subject.setreinforcementMsg("you can't place reinforcement armies greater than current reinforcement armies");
                System.out.println("you can't place reinforcement armies greater than current reinforcement armies");
            } else {
                subject.setreinforcementMsg("Enter the Territory Name on which you want to add Reinforcement army");
                System.out.println("Enter the Territory Name on which you want to add Reinforcement army");
                Scanner sci = new Scanner(System.in);
                String territoryName = sci.nextLine();
                if (territoriesList.contains(territoryName)) {
                    int index = territoriesList.indexOf(territoryName);
                    int value = armiesList.get(index);
                    value = value + placeArmies;
                    System.out.println("updated armies " + value);
                    players.setAssignedArmies(currentPlayerName, index, value);
                    reinforcementArmy = reinforcementArmy - placeArmies;
                    System.out.println(playersOwnedTerritories);
                    System.out.println(assignedArmies);

                } else {
                    subject.setreinforcementMsg("please enter valid Territory Name");
                    System.out.println("please enter valid Territory Name");
                }
            }
            }
        }
    }

    public String getPlayerRandomArmyTerritory(String currentPlayerName) {
        String territoryName=null;
        int size=assignedArmies.get(currentPlayerName).size();
        int index=(int )(Math.random() * size);
        territoryName=playersOwnedTerritories.get(currentPlayerName).get(index);
        return territoryName;
    }

    public String getPlayerHighestArmyTerritory(String currentPlayerName) {
        String territoryName=null;
        ArrayList<Integer> armies=assignedArmies.get(currentPlayerName);
        int highestArmy=Collections.max(armies);
        int index=armies.indexOf(highestArmy);
        territoryName=playersOwnedTerritories.get(currentPlayerName).get(index);
        return territoryName;
    }
    public String getPlayerWeakestArmyTerritory(String currentPlayerName) {
        String territoryName=null;
        ArrayList<Integer> armies=assignedArmies.get(currentPlayerName);
        int weakestArmy=Collections.min(armies);
        int index=armies.indexOf(weakestArmy);
        territoryName=playersOwnedTerritories.get(currentPlayerName).get(index);
        return territoryName;
    }

    /**
 * This function is to run attack phase
 * @param players:object of Player class
 * @param territory:object of Territory class
 * @param currentPlayerName:name of current player
 * @param subject:object of Subject class
 * @return army capture count
 */
    public int attackPhase(Territory territory, String currentPlayerName, Players players, Subject subject) {
        int attackCaptureCount=0;
        boolean isAttackWon=false;
        boolean isAttack = false;
        int option = 0;
        System.out.println("Press 1 if you want to do Attack \n Press 2 to exit from attack phase");
        Scanner t= new Scanner(System.in);
        int optionAttack= t.nextInt();
        if(optionAttack==2){
            isAttack=true;
            System.out.println("Attack phase skipped");
        }
        //check attack is possible
        boolean checkIsAttackPossible= players.checkIsAttackPossible(currentPlayerName);
        if(checkIsAttackPossible==false){
            isAttack=true;
            System.out.println("You can't attack because each territory is having 1 army only");
        }
        while (!isAttack) {

            System.out.println("------Attack Phase in Progress------");
            subject.setPhase("------Attack Phase in Progress------");
            System.out.println("Enter the territory name from which you want to attack");
            subject.setattackMsg("Enter the territory name from which you want to attack");
            Scanner sc = new Scanner(System.in);
            String attackerTerritory = sc.nextLine();

            //to check if player owns that territory
            List<String> territoryList = null;
            HashMap<String, Integer> adjacentList = null;
            if (players.playersOwnedTerritories.get(currentPlayerName).contains(attackerTerritory)) {
                int attackerArmy= getTerritoryArmy(attackerTerritory);
                if(attackerArmy>=2) {
                    territoryList = territory.getAdjacentTerritoryList(attackerTerritory);

                    adjacentList = new HashMap<>();
                    for (int i = 0; i < territoryList.size(); i++) {
                        String territoryData = territoryList.get(i);
                        for (int j = 0; j < players.playersNameList.size(); j++) {
                            List<String> territorylist = getPlayerOwnedTerritories(players.playersNameList.get(j));
                            ArrayList<Integer> armiesList = players.assignedArmies.get((players.playersNameList.get(j)));

                            if (territorylist.contains(territoryData)) {
                                int index = territorylist.indexOf(territoryData);
                                int armies = armiesList.get(index);
                                if (playersNameList.get(j) != currentPlayerName) 
                                {
                                	subject.setattackMsg("Territory: " + territoryData + " ,Territory owned by: " + playersNameList.get(j) + "  No. of Armies present-" + armies);
                                    System.out.println("Territory: " + territoryData + " ,Territory owned by: " + playersNameList.get(j) + "  No. of Armies present-" + armies);
                                    adjacentList.put(territoryData, armies);
                                }
                            }

                        }
                    }
                    subject.setattackMsg("Enter the territory name which you will be attacking");
                    System.out.println("Enter the territory name which you will be attacking");
                    String attackedTerritory = sc.nextLine();
                    //to check if player entered the adjacent territory name

                    if (adjacentList.containsKey(attackedTerritory)) {

                        System.out.println("Press 1 for all-out mode\n Press 2 for normal mode");
                        Scanner all = new Scanner(System.in);
                        int isAllOut = all.nextInt();
                        if (isAllOut == 1) {
                           isAttackWon= players.doAllOutAttack(currentPlayerName,attackerTerritory,attackedTerritory, players, subject);
                           System.out.println(isAttackWon);
                           if(isAttackWon){
                               attackCaptureCount++;
                           }
                            boolean checkIsAttackpossible = players.checkIsAttackPossible(currentPlayerName);
                            if (checkIsAttackpossible == false) {
                                isAttack = true;
                                System.out.println("You can't attack more because each territory is having 1 army only");
                                subject.setattackMsg("You can't attack more because each territory is having 1 army only");
                            } else {
                                System.out.println("Press 1 to continue in attack phaase \n Press 2 to exit from attack phase");
                                subject.setattackMsg("You can't attack more because each territory is having 1 army only");
                                Scanner inp = new Scanner(System.in);
                                int inputAttack = inp.nextInt();
                                if (inputAttack == 1) {
                                    isAttack = false;
                                } else {
                                    isAttack = true;
                                }
                            }
                        }
                        else{
                        subject.setattackMsg("Enter the No. of Dice Attacker Player wants to roll?");
                        System.out.println("Enter the No. of Dice Attacker Player wants to roll?");
                        Scanner i = new Scanner(System.in);
                        int attackerDice = i.nextInt();
                        subject.setattackMsg("Enter the No. of Dice Attacker Player wants to roll?");
                        System.out.println("Enter the No. of Dice Attacked Player wants to roll?");
                        int attackedDice = i.nextInt();
                        ArrayList<Integer> attackerList = new ArrayList<>();
                        ArrayList<Integer> attackedList = new ArrayList<>();

                        int attackedArmy = getTerritoryArmy(attackedTerritory);
                        if (attackerArmy >= attackerDice + 1 && attackerDice <= 3) {
                            if (attackedArmy >= attackedDice && attackedDice <= 2) {
                                for (int j = 0; j < attackerDice; j++) {
                                    int dice = (int) (Math.random() * 6 + 1);
                                    subject.setattackMsg("Attacker Player-" + j + 1 + " Dice Roll-" + dice);
                                    System.out.println("Attacker Player-" + j + 1 + " Dice Roll-" + dice);
                                    attackerList.add(dice);
                                }

                                for (int k = 0; k < attackedDice; k++) {
                                    int dice = (int) (Math.random() * 6 + 1);
                                    subject.setattackMsg("Attacker Player-" + k + 1 + " Dice Roll-" + dice);
                                    System.out.println("Attacked Player-" + k + 1 + " Dice Roll-" + dice);
                                    attackedList.add(dice);
                                }

                                Collections.sort(attackerList);
                                Collections.sort(attackedList);
                                Collections.reverse(attackerList);
                                Collections.reverse(attackedList);
                                System.out.println(attackerList);
                                System.out.println(attackedList);

                                String attackedPlayerName = null;
                                int attackedTerritoryIndex = 0;
                                int attackerTerritoryIndex = 0;
                                // check first dice roll
                                if (attackedList.get(0) >= attackerList.get(0)) {
                                    subject.setattackMsg("Attacked Player won");
                                    System.out.println("Attacked Player won");
                                    //get player name of attacked territory
                                    //if attacked player won, then subtract the army from attacker player
                                    attackerTerritoryIndex = players.getTerritoryIndex(attackerTerritory, currentPlayerName);
                                    System.out.println(attackerTerritoryIndex);
                                    attackerArmy = attackerArmy - 1;
                                    players.setAssignedArmies(currentPlayerName, attackerTerritoryIndex, attackerArmy);
                                    System.out.println(playersOwnedTerritories);
                                    System.out.println(assignedArmies);


                                } else {
                                    subject.setattackMsg("Attacker Player won");
                                    System.out.println("Attacker Player won");
                                    attackedPlayerName = players.getTerritoryPlayerName(attackedTerritory);
                                    attackedTerritoryIndex = players.getTerritoryIndex(attackedTerritory, attackedPlayerName);
                                    System.out.println(attackedTerritoryIndex);
                                    attackedArmy = attackedArmy - 1;
                                    players.setAssignedArmies(attackedPlayerName, attackedTerritoryIndex, attackedArmy);
                                    System.out.println(playersOwnedTerritories);
                                    System.out.println(assignedArmies);

                                }

                                //now to check if attacked army is 0 or not
                                int afterAttackArmy = players.getTerritoryArmy(attackedTerritory);
                                if (afterAttackArmy == 0) {
                                    players.setTerritoryToAttacker(currentPlayerName, attackedPlayerName, attackedTerritory, attackedTerritoryIndex);
                                    System.out.println("Attacker captures the Territory");
                                    subject.setattackMsg("Attacker captures the Territory");
                                    attackCaptureCount++;
                                    boolean isArmyMoved = false;
                                    while (!isArmyMoved) {

                                        System.out.println("Enter no. of armies you want to move from " + attackerTerritory + " to " + attackedTerritory);
                                        subject.setattackMsg("Enter no. of armies you want to move from " + attackerTerritory + " to " + attackedTerritory);
                                        Scanner sn = new Scanner(System.in);
                                        int moveArmy = sn.nextInt();

                                        isArmyMoved = players.setArmyToAttackedTerritory(currentPlayerName, attackedTerritory, attackerTerritory, moveArmy);
                                    }

                                    boolean isLost=players.checkPlayerLost(attackedPlayerName);
                                    if(isLost){
                                        players.getLostPlayerCards(currentPlayerName,attackedPlayerName);
                                    }

                                } else {

                                    //now to check if both have 2nd dice available
                                    if (attackedDice > 1 && attackerDice > 1) {
                                        if (attackedList.get(1) >= attackerList.get(1)) {
                                            subject.setattackMsg("Attacked Player won");
                                            System.out.println("Attacked Player won");
                                            //get player name of attacked territory
                                            //if attacked player won, then subtract the army from attacker player
                                            // int attackerTerritoryIndex = players.getTerritoryIndex(attackerTerritory, currentPlayerName);
                                            attackerArmy = attackerArmy - 1;
                                            players.setAssignedArmies(currentPlayerName, attackerTerritoryIndex, attackerArmy);
                                            System.out.println(playersOwnedTerritories);
                                            System.out.println(assignedArmies);


                                        } else {
                                            subject.setattackMsg("Attacker Player won");
                                            System.out.println("Attacker Player won");
                                            String attackedPlayername = players.getTerritoryPlayerName(attackedTerritory);
                                            int attackedTerritoryindex = players.getTerritoryIndex(attackedTerritory, attackedPlayername);
                                            attackedArmy = attackedArmy - 1;
                                            players.setAssignedArmies(attackedPlayerName, attackedTerritoryindex, attackedArmy);
                                            System.out.println(playersOwnedTerritories);
                                            System.out.println(assignedArmies);

                                            //now to check if attacked army is 0 or not
                                            int afterAttackArmies = players.getTerritoryArmy(attackedTerritory);
                                            if (afterAttackArmies == 0) {
                                                players.setTerritoryToAttacker(currentPlayerName, attackedPlayername, attackedTerritory, attackedTerritoryindex);
                                                System.out.println("Attacker captures the Territory");
                                                subject.setattackMsg("Attacker captures the Territory");
                                                attackCaptureCount++;
                                                boolean isArmyMoved = false;
                                                while (!isArmyMoved) {

                                                    System.out.println("Enter no. of armies you want to move from " + attackerTerritory + " to " + attackedTerritory);
                                                    subject.setattackMsg("Enter no. of armies you want to move from " + attackerTerritory + " to " + attackedTerritory);
                                                    Scanner sn = new Scanner(System.in);
                                                    int moveArmy = sn.nextInt();

                                                    isArmyMoved = players.setArmyToAttackedTerritory(currentPlayerName, attackedTerritory, attackerTerritory, moveArmy);
                                                }

                                                boolean isLost=players.checkPlayerLost(attackedPlayerName);
                                                if(isLost){
                                                    players.getLostPlayerCards(currentPlayerName,attackedPlayerName);
                                                }

                                            }

                                        }
                                    }
                                }
                                boolean checkIsAttackpossible = players.checkIsAttackPossible(currentPlayerName);
                                if (checkIsAttackpossible == false) {
                                    isAttack = true;
                                    System.out.println("You can't attack because each territory is having 1 army only");
                                    subject.setattackMsg("You can't attack because each territory is having 1 army only");
                                } else {
                                    System.out.println("Press 1 to continue in attack phaase \n Press 2 to exit from attack phase");
                                    subject.setattackMsg("Press 1 to continue in attack phaase \n Press 2 to exit from attack phase");
                                    Scanner inp = new Scanner(System.in);
                                    int inputAttack = inp.nextInt();
                                    if (inputAttack == 1) {
                                        isAttack = false;
                                    } else {
                                        isAttack = true;
                                    }
                                }


                            } else {
                                subject.setattackMsg("Attacked player can't roll this no. of dice");
                                System.out.println("Attacked player can't roll this no. of dice");
                            }
                        } else {
                            subject.setattackMsg("Attacker player can't roll this no. of dice");
                            System.out.println("Attacker player can't roll this no. of dice");
                        }
                    }

                    } else 
                    {
                    	subject.setattackMsg("Only enter that territory name which is adjacent");
                        System.out.println("Only enter that territory name which is adjacent");
                    }
                }
                else
                {
                	subject.setattackMsg("Territory Army should be greater than 2");
                    System.out.println("Territory Army should be greater than 2");
                }




            }
            else
            {
            	subject.setattackMsg("Only enter the territory name which you own");
                System.out.println("Only enter the territory name which you own");
            }

            System.out.println(playersOwnedTerritories);
            System.out.println(assignedArmies);




        }
        return attackCaptureCount;

    }
/**
 * This method is used to do all out attack
 * @param currentPlayerName:name of current player
 * @param attackerTerritory:Territory which is attacking
 * @param attackedTerritory:Territory which is been attacked
 * @param players:object of Player class
 * @param subject:object of Subject class
 * @return true if all out was successfull else return false
 */
    public boolean doAllOutAttack(String currentPlayerName, String attackerTerritory, String attackedTerritory, Players players, Subject subject) {
        boolean isAttackWon=false;
        boolean isAttack=false;
        while (!isAttack) {
            int attackerDice;
            int attackedDice;
            ArrayList<Integer> attackerList = new ArrayList<>();
            ArrayList<Integer> attackedList = new ArrayList<>();

            int attackerArmy = players.getTerritoryArmy(attackerTerritory);
            int attackedArmy = players.getTerritoryArmy(attackedTerritory);
            if (attackerArmy >= 2){
                if (attackerArmy > 3) {
                    attackerDice = 3;
                } else if (attackerArmy > 2) {
                    attackerDice = 2;
                } else {
                    attackerDice = 1;
                }

            if (attackedArmy > 1) {
                attackedDice = 2;
            } else {
                attackedDice = 1;
            }

            if (attackerArmy >= attackerDice + 1 && attackerDice <= 3) {
                if (attackedArmy >= attackedDice && attackedDice <= 2) {
                    for (int j = 0; j < attackerDice; j++) {
                        int dice = (int) (Math.random() * 6 + 1);
                        subject.setattackMsg("Attacker Player-" + j + 1 + " Dice Roll-" + dice);
                        System.out.println("Attacker Player-" + j + 1 + " Dice Roll-" + dice);
                        attackerList.add(dice);
                    }

                    for (int k = 0; k < attackedDice; k++) {
                        int dice = (int) (Math.random() * 6 + 1);
                        subject.setattackMsg("Attacker Player-" + k + 1 + " Dice Roll-" + dice);
                        System.out.println("Attacked Player-" + k + 1 + " Dice Roll-" + dice);
                        attackedList.add(dice);
                    }

                    Collections.sort(attackerList);
                    Collections.sort(attackedList);
                    Collections.reverse(attackerList);
                    Collections.reverse(attackedList);
                    System.out.println(attackerList);
                    System.out.println(attackedList);

                    String attackedPlayerName = null;
                    int attackedTerritoryIndex = 0;
                    int attackerTerritoryIndex = 0;
                    // check first dice roll
                    if (attackedList.get(0) >= attackerList.get(0)) {
                        subject.setattackMsg("Attacked Player won");
                        System.out.println("Attacked Player won");
                        //get player name of attacked territory
                        //if attacked player won, then subtract the army from attacker player
                        attackerTerritoryIndex = players.getTerritoryIndex(attackerTerritory, currentPlayerName);
                        System.out.println(attackerTerritoryIndex);
                        attackerArmy = attackerArmy - 1;
                        players.setAssignedArmies(currentPlayerName, attackerTerritoryIndex, attackerArmy);
                        System.out.println(playersOwnedTerritories);
                        System.out.println(assignedArmies);


                    } else {
                        subject.setattackMsg("Attacker Player won");
                        System.out.println("Attacker Player won");
                        attackedPlayerName = players.getTerritoryPlayerName(attackedTerritory);
                        attackedTerritoryIndex = players.getTerritoryIndex(attackedTerritory, attackedPlayerName);
                        System.out.println(attackedTerritoryIndex);
                        attackedArmy = attackedArmy - 1;
                        players.setAssignedArmies(attackedPlayerName, attackedTerritoryIndex, attackedArmy);
                        System.out.println(playersOwnedTerritories);
                        System.out.println(assignedArmies);

                    }

                    //now to check if attacked army is 0 or not
                    int afterAttackArmy = players.getTerritoryArmy(attackedTerritory);
                    if (afterAttackArmy == 0) {
                        players.setTerritoryToAttacker(currentPlayerName, attackedPlayerName, attackedTerritory, attackedTerritoryIndex);
                        System.out.println("Attacker captures the Territory");
                        isAttackWon=true;
                        boolean isArmyMoved = false;
                        while (!isArmyMoved) {

                            System.out.println("Enter no. of armies you want to move from " + attackerTerritory + " to " + attackedTerritory);
                            Scanner sn = new Scanner(System.in);
                            int moveArmy = sn.nextInt();

                            isArmyMoved = players.setArmyToAttackedTerritory(currentPlayerName, attackedTerritory, attackerTerritory, moveArmy);
                            isAttack = true;
                        }

                        boolean isLost=players.checkPlayerLost(attackedPlayerName);
                        if(isLost){
                            players.getLostPlayerCards(currentPlayerName,attackedPlayerName);
                        }

                    } else {

                        //now to check if both have 2nd dice available
                        if (attackedDice > 1 && attackerDice > 1) {
                            if (attackedList.get(1) >= attackerList.get(1)) {
                                subject.setattackMsg("Attacked Player won");
                                System.out.println("Attacked Player won");
                                //get player name of attacked territory
                                //if attacked player won, then subtract the army from attacker player
                                // int attackerTerritoryIndex = players.getTerritoryIndex(attackerTerritory, currentPlayerName);
                                attackerArmy = attackerArmy - 1;
                                players.setAssignedArmies(currentPlayerName, attackerTerritoryIndex, attackerArmy);
                                System.out.println(playersOwnedTerritories);
                                System.out.println(assignedArmies);


                            } else {
                                subject.setattackMsg("Attacker Player won");
                                System.out.println("Attacker Player won");
                                String attackedPlayername = players.getTerritoryPlayerName(attackedTerritory);
                                int attackedTerritoryindex = players.getTerritoryIndex(attackedTerritory, attackedPlayername);
                                attackedArmy = attackedArmy - 1;
                                players.setAssignedArmies(attackedPlayername, attackedTerritoryindex, attackedArmy);
                                System.out.println(playersOwnedTerritories);
                                System.out.println(assignedArmies);

                                //now to check if attacked army is 0 or not
                                int afterAttackArmies = players.getTerritoryArmy(attackedTerritory);
                                if (afterAttackArmies == 0) {
                                    players.setTerritoryToAttacker(currentPlayerName, attackedPlayername, attackedTerritory, attackedTerritoryindex);
                                    System.out.println("Attacker captures the Territory");
                                    isAttackWon=true;
                                    boolean isArmyMoved = false;
                                    while (!isArmyMoved) {

                                        System.out.println("Enter no. of armies you want to move from " + attackerTerritory + " to " + attackedTerritory);
                                        Scanner sn = new Scanner(System.in);
                                        int moveArmy = sn.nextInt();

                                        isArmyMoved = players.setArmyToAttackedTerritory(currentPlayerName, attackedTerritory, attackerTerritory, moveArmy);
                                        isAttack = true;
                                    }

                                    boolean isLost=players.checkPlayerLost(attackedPlayerName);
                                    if(isLost){
                                        players.getLostPlayerCards(currentPlayerName,attackedPlayerName);
                                    }

                                }

                            }
                        }
                    }
                    /*boolean checkIsAttackpossible = players.checkIsAttackPossible(currentPlayerName);
                    if (checkIsAttackpossible == false) {
                        isAttack = true;
                        System.out.println("You can't attack because each territory is having 1 army only");
                    } else {
                        System.out.println("Press 1 to continue in attack phaase \n Press 2 to exit from attack phase");
                        Scanner inp = new Scanner(System.in);
                        int inputAttack = inp.nextInt();
                        if (inputAttack == 1) {
                            isAttack = false;
                        } else {
                            isAttack = true;
                        }
                    }*/


                }
            }
        }
            else{
                System.out.println("Can't attack lost all armies");
                isAttack=true;
            }
        }
        return isAttackWon;
    }

/**
 * This method provides code for fortification phase
 * @param option:provide option
 * @param currentPlayerName:name of current player
 * @param players:object of player class
 * @param reinforcement:object of Reinforcement class
 * @param territory:object of Territory class
 * @param subject:object of subject class
 */
    public boolean fortificationPhase(int option, String currentPlayerName, Players players, Reinforcement reinforcement, Territory territory, Subject subject){
        boolean isFortification=false;
        if (option == 1) {
            boolean flag = true;
            subject.setPhase("------Fortification Phase Started--------");
            System.out.println("------Fortification Phase Started--------");

            ArrayList<String> territoriesList = players.getPlayerOwnedTerritories(currentPlayerName);
            ArrayList<Integer> armiesList = players.getPlayerAssignedArmies(currentPlayerName);
            int noOfTerritories = territoriesList.size();
            subject.setfortificationMsg("Number of Territories Owned " + noOfTerritories);
            System.out.println("Number of Territories Owned " + noOfTerritories);
            for (int j = 0; j < noOfTerritories; j++) {
                System.out.println(territoriesList.get(j) + " - " + armiesList.get(j));
            }
            while (flag) {
                if (currentPlayerName.equals("AGGRESSIVE")) {
                    String destinationTerritoryName = players.getPlayerHighestArmyTerritory(currentPlayerName);
                    String initialTerritoryName = players.getConnectedHighestArmyTerritory(currentPlayerName, destinationTerritoryName, players, territory);
                    System.out.println(initialTerritoryName);
                    if (initialTerritoryName.equals("NoTerritoryFound")) {
                        flag=false;
                    } else {
                        int indexInitialTerritory = territoriesList.indexOf(initialTerritoryName);
                        int valueInitialTerritory = armiesList.get(indexInitialTerritory);
                        int fortificationArmiesValue = valueInitialTerritory - 1;
                        flag = false;
                        int indexDestinationTerritory = territoriesList.indexOf(destinationTerritoryName);
                        int valueDestinationTerritory = armiesList.get(indexDestinationTerritory);
                        valueDestinationTerritory = valueDestinationTerritory + fortificationArmiesValue;
                        players.setAssignedArmies(currentPlayerName, indexDestinationTerritory, valueDestinationTerritory);
                        valueInitialTerritory = valueInitialTerritory - fortificationArmiesValue;
                        players.setAssignedArmies(currentPlayerName, indexInitialTerritory, valueInitialTerritory);
                        subject.setfortificationMsg("---Fortification phase-successfull--");
                        System.out.println("---Fortification phase-successfull--");

                        ArrayList<String> territoriesLists = players.getPlayerOwnedTerritories(currentPlayerName);
                        ArrayList<Integer> armiesLists = players.getPlayerAssignedArmies(currentPlayerName);
                        int noOfTerritory = territoriesLists.size();
                        subject.setfortificationMsg("Number of Territories Owned " + noOfTerritory);
                        System.out.println("Number of Territories Owned " + noOfTerritory);
                        for (int j = 0; j < noOfTerritory; j++) {
                            System.out.println(territoriesLists.get(j) + " - " + armiesLists.get(j));
                        }
                        subject.setfortificationMsg("--------Turn Ended--------");
                        System.out.println("--------Turn Ended--------");
                    }
                }
                else if(currentPlayerName.equals("BENEVOLENT")){
                    String destinationTerritoryName=players.getPlayerWeakestArmyTerritory(currentPlayerName);
                    System.out.println(destinationTerritoryName);
                    String initialTerritoryName=players.getConnectedHighestArmyTerritory(currentPlayerName, destinationTerritoryName, players, territory);
                    System.out.println(initialTerritoryName);
                    if (initialTerritoryName.equals("NoTerritoryFound")) {
                        flag=false;
                    } else {

                        int indexInitialTerritory = territoriesList.indexOf(initialTerritoryName);
                        int valueInitialTerritory = armiesList.get(indexInitialTerritory);
                        int fortificationArmiesValue = valueInitialTerritory - 1;
                        flag = false;
                        int indexDestinationTerritory = territoriesList.indexOf(destinationTerritoryName);
                        int valueDestinationTerritory = armiesList.get(indexDestinationTerritory);
                        valueDestinationTerritory = valueDestinationTerritory + fortificationArmiesValue;
                        players.setAssignedArmies(currentPlayerName, indexDestinationTerritory, valueDestinationTerritory);
                        valueInitialTerritory = valueInitialTerritory - fortificationArmiesValue;
                        players.setAssignedArmies(currentPlayerName, indexInitialTerritory, valueInitialTerritory);
                        subject.setfortificationMsg("---Fortification phase-successfull--");
                        System.out.println("---Fortification phase-successfull--");

                        ArrayList<String> territoriesLists = players.getPlayerOwnedTerritories(currentPlayerName);
                        ArrayList<Integer> armiesLists = players.getPlayerAssignedArmies(currentPlayerName);
                        int noOfTerritory = territoriesLists.size();
                        subject.setfortificationMsg("Number of Territories Owned " + noOfTerritory);
                        System.out.println("Number of Territories Owned " + noOfTerritory);
                        for (int j = 0; j < noOfTerritory; j++) {
                            System.out.println(territoriesLists.get(j) + " - " + armiesLists.get(j));
                        }
                        subject.setfortificationMsg("--------Turn Ended--------");
                        System.out.println("--------Turn Ended--------");
                    }
                }
                else if(currentPlayerName.equals("RANDOM")){

                }
                else if(currentPlayerName.equals("CHEATER")){

                }
                else{
                    subject.setfortificationMsg("Enter the Territory Name from which you want to move armies");
                System.out.println("Enter the Territory Name from which you want to move armies");
                Scanner sct = new Scanner(System.in);
                String initialTerritoryName = sct.nextLine();
                subject.setfortificationMsg("Enter the no. of armies you want to move");
                System.out.println("Enter the no. of armies you want to move");
                int fortificationArmiesValue = sct.nextInt();
                subject.setfortificationMsg("Enter the Territory Name where you want armies to be moved");
                System.out.println("Enter the Territory Name where you want armies to be moved");
                Scanner scd = new Scanner(System.in);
                String destinationTerritoryName = scd.nextLine();
                if (territoriesList.contains(initialTerritoryName) && territoriesList.contains(destinationTerritoryName)) {
                    boolean connectedAdjacentResult = players.checkConnectedAdjacent(initialTerritoryName, destinationTerritoryName, currentPlayerName, territory);
                    if (!connectedAdjacentResult) {
                        subject.setfortificationMsg("No path between the Territories");
                        System.out.println("No path between the Territories");
                    } else {
                        int indexInitialTerritory = territoriesList.indexOf(initialTerritoryName);
                        int valueInitialTerritory = armiesList.get(indexInitialTerritory);
                        if (fortificationArmiesValue <= valueInitialTerritory - 1) {
                            flag = false;
                            int indexDestinationTerritory = territoriesList.indexOf(destinationTerritoryName);
                            int valueDestinationTerritory = armiesList.get(indexDestinationTerritory);
                            valueDestinationTerritory = valueDestinationTerritory + fortificationArmiesValue;
                            players.setAssignedArmies(currentPlayerName, indexDestinationTerritory, valueDestinationTerritory);
                            valueInitialTerritory = valueInitialTerritory - fortificationArmiesValue;
                            players.setAssignedArmies(currentPlayerName, indexInitialTerritory, valueInitialTerritory);
                            subject.setfortificationMsg("---Fortification phase-successfull--");
                            System.out.println("---Fortification phase-successfull--");

                            ArrayList<String> territoriesLists = players.getPlayerOwnedTerritories(currentPlayerName);
                            ArrayList<Integer> armiesLists = players.getPlayerAssignedArmies(currentPlayerName);
                            int noOfTerritory = territoriesLists.size();
                            subject.setfortificationMsg("Number of Territories Owned " + noOfTerritory);
                            System.out.println("Number of Territories Owned " + noOfTerritory);
                            for (int j = 0; j < noOfTerritory; j++) {
                                System.out.println(territoriesLists.get(j) + " - " + armiesLists.get(j));
                            }
                            subject.setfortificationMsg("--------Turn Ended--------");
                            System.out.println("--------Turn Ended--------");
                        } else {
                            subject.setfortificationMsg("You can only add armies upto");
                            System.out.println("You can only add armies upto");
                            System.out.println(valueInitialTerritory - 1);
                        }
                    }

                    // players.setAssignedArmies(currentPlayerName, index, value);
                } else {
                    subject.setfortificationMsg("please enter valid Territory Name");
                    System.out.println("please enter valid Territory Name");
                }
            }
            }
            isFortification=true;
        }
        //option 2 or other
        else 
        {
        	subject.setfortificationMsg("--------Turn Ended--------");
            System.out.println("--------Turn Ended--------");
        }
        return isFortification;
    }

    public String getConnectedHighestArmyTerritory(String currentPlayerName, String initialTerritoryName, Players players, Territory territory) {
        String destinationTerritory="";
        ArrayList<Integer> aggressiveTerritoryArmies=assignedArmies.get(currentPlayerName);
        int indexInitialTerritoryName=playersOwnedTerritories.get(currentPlayerName).indexOf(initialTerritoryName);
        int armies=0;
        String destinationTerritoryName="";
        for(int i=0; i<assignedArmies.size();i++) {
            destinationTerritoryName=playersOwnedTerritories.get(currentPlayerName).get(i);
            System.out.println(destinationTerritoryName);
            boolean connectedAdjacentResult = players.checkConnectedAdjacent(initialTerritoryName, destinationTerritoryName, currentPlayerName, territory);
            System.out.println(connectedAdjacentResult);
            if (connectedAdjacentResult&&(!destinationTerritoryName.equals(initialTerritoryName))) {
                int territoryArmy=aggressiveTerritoryArmies.get(i);
               if(territoryArmy>armies){
                   System.out.println(armies);
                   System.out.println(destinationTerritory);
                   armies=territoryArmy;
                   destinationTerritory=destinationTerritoryName;
               }
            }
        }
        System.out.println(destinationTerritory);
        if(destinationTerritory.equals("")){
            destinationTerritory="NoTerritoryFound";
        }
        return destinationTerritory;
    }

    /**
 * This method runs code for card View
 * @param currentPlayerName:name of current player
 * @param players:object of player class
 * @param territory:object of Territory class
 * @param exchangeSubject:Object of Exchange Subject Class
 * @return count of reinforcement army
 */
    public int cardView(String currentPlayerName, Players players, Territory territory,ExchangeSubject exchangeSubject)
    {   int reinforcementArmy=0;
        System.out.println("--------Card View Phase---------");
        exchangeSubject.setcardsOwned("--------Card View Phase---------");
        List<String> playerCardList=players.playersOwnedCards.get(currentPlayerName);
        int size=playerCardList.size();
        System.out.println(size);
        System.out.println(playersOwnedCards);
        if(size>=5) {
            boolean cardResult = false;
            while (!cardResult) {

                //call to exchange cards
                for (int i = 0; i < size; i++) {
                    System.out.println("Press-" + i + "to exchange this card-" + playerCardList.get(i));
                    exchangeSubject.setcardsOwned("Press-" + i+ "to exchange this card-" + playerCardList.get(i));
                }
                System.out.println("Enter the first card no. ");
                exchangeSubject.setcardsOwned("Enter the first card no. ");
                Scanner input = new Scanner(System.in);
                int option1 = input.nextInt();
                System.out.println("Enter the second card no. ");
                exchangeSubject.setcardsOwned("Enter the second card no. ");
                int option2 = input.nextInt();
                System.out.println("Enter the third card no. ");
                exchangeSubject.setcardsOwned("Enter the third card no. ");
                int option3 = input.nextInt();

                String option_1 = playerCardList.get(option1);
                String option_2 = playerCardList.get(option2);
                String option_3 = playerCardList.get(option3);
                cardResult = players.checkCardValid(option_1, option_2, option_3);
                if(cardResult==true){
                    //then to remove cards list from the player owned cards and then add back to the available cards
                    players.playersOwnedCards.get(currentPlayerName).remove(option1);
                    players.playersOwnedCards.get(currentPlayerName).remove(option2);
                    players.playersOwnedCards.get(currentPlayerName).remove(option3);

                    //add in the territorycards list
                    String[] option_1Array=option_1.split("-");
                    String[] option_2Array=option_2.split("-");
                    String[] option_3Array=option_3.split("-");
                    territory.territoryCard.put(option_1Array[0],option_1Array[1]);
                    territory.territoryCard.put(option_2Array[0],option_2Array[1]);
                    territory.territoryCard.put(option_3Array[0],option_3Array[1]);

                    //also successful card return means to give reinforcement army
                    int army=Players.cardsArmy;
                    System.out.println("previous army recieve by the card exchange "+army);
                    exchangeSubject.setcardsOwned("previous army recieve by the card exchange "+army);
                    Players.cardsArmy=army+5;
                    reinforcementArmy=Players.cardsArmy;
                    System.out.println("current army recieve by the card exchange "+reinforcementArmy);
                    exchangeSubject.setcardsOwned("current army recieve by the card exchange "+reinforcementArmy);
                }
                else{
                    System.out.println("Enter valid cards option pattern");
                }

            }
        }
        else if(size>=3){
            System.out.println("You Can exchange cards for the army\n press 1 to exchange cards, press 2 to exit");
            exchangeSubject.setcardsOwned("You Can exchange cards for the army\n press 1 to exchange cards, press 2 to exit");
            Scanner sc=new Scanner(System.in);
            int isexchange= sc.nextInt();
            if(isexchange==1){

                boolean cardResult = false;
                while (!cardResult) {

                    //call to exchange cards
                    for (int i = 0; i < size; i++) {
                        System.out.println("Press-" + i + "to exchange this card-" + playerCardList.get(i));
                    }
                    System.out.println("Enter the first card no. ");
                    Scanner input = new Scanner(System.in);
                    int option1 = input.nextInt();
                    System.out.println("Enter the second card no. ");
                    int option2 = input.nextInt();
                    System.out.println("Enter the third card no. ");
                    int option3 = input.nextInt();

                    String option_1 = playerCardList.get(option1);
                    String option_2 = playerCardList.get(option2);
                    String option_3 = playerCardList.get(option3);
                    cardResult = players.checkCardValid(option_1, option_2, option_3);
                    if(cardResult==true){
                        //then to remove cards list from the player owned cards and then add back to the available cards
                        players.playersOwnedCards.get(currentPlayerName).remove(option_1);
                        players.playersOwnedCards.get(currentPlayerName).remove(option_2);
                        players.playersOwnedCards.get(currentPlayerName).remove(option_3);

                        //add in the territorycards list
                        String[] option_1Array=option_1.split("-");
                        String[] option_2Array=option_2.split("-");
                        String[] option_3Array=option_3.split("-");
                        territory.territoryCard.put(option_1Array[0],option_1Array[1]);
                        territory.territoryCard.put(option_2Array[0],option_2Array[1]);
                        territory.territoryCard.put(option_3Array[0],option_3Array[1]);

                        //also successful card return means to give reinforcement army
                        int army=Players.cardsArmy;
                        System.out.println("previous army recieve by the card exchange "+army);
                        Players.cardsArmy=army+5;
                        reinforcementArmy=Players.cardsArmy;
                        System.out.println("current army recieve by the card exchange "+reinforcementArmy);

                    }
                    else{
                        System.out.println("not valid cards");
                        cardResult=true;
                    }

                }

            }
            else{
                System.out.println("----Exiting Card Phase View------");
            }

        }
        else{
            System.out.println("You don't have enough cards to exchange");
        }
        return reinforcementArmy;
    }
/**
 * This method check for card validity
 * @param option_1:option1
 * @param option_2:option2
 * @param option_3:option3
 * @return true if card is valid else return false
 */
    public boolean checkCardValid(String option_1, String option_2, String option_3) {
        boolean isCardValid=false;
        option_1= StringUtils.substringAfter(option_1,"-");
        option_2=StringUtils.substringAfter(option_2,"-");
        option_3=StringUtils.substringAfter(option_3,"-");

        //first pattern-all same card
        if(option_1.equals("infantry")){
            if(option_2.equals("infantry")){
                if(option_3.equals("infantry")){
                    isCardValid=true;
                }
            }
        }

        //second pattern-all same card
        if(option_1.equals("cavalry")){
            if(option_2.equals("cavalry")){
                if(option_3.equals("cavalry")){
                    isCardValid=true;
                }
            }
        }

        //third pattern-all same card
        if(option_1.equals("artillery")){
            if(option_2.equals("artillery")){
                if(option_3.equals("artillery")){
                    isCardValid=true;
                }
            }
        }

        //fourth pattern-all different card
        if(option_1.equals("infantry")) {
            if (option_2.equals("cavalry")) {
                if (option_3.equals("artillery")) {
                    isCardValid = true;
                }
            }
        }

        //fifth pattern-all different card
        if(option_1.equals("infantry")) {
            if (option_2.equals("artillery")) {
                if (option_3.equals("cavalry")) {
                    isCardValid = true;
                }
            }
        }


        //sixth pattern-all different card
        if(option_1.equals("cavalry")) {
            if (option_2.equals("infantry")) {
                if (option_3.equals("artillery")) {
                    isCardValid = true;
                }
            }
        }
       //seventh pattern-all different card
        if(option_1.equals("cavalry")) {
            if (option_2.equals("artillery")) {
                if (option_3.equals("infantry")) {
                    isCardValid = true;
                }
            }
        }
        //8th pattern-all different card
        if(option_1.equals("artillery")) {
            if (option_2.equals("cavalry")) {
                if (option_3.equals("infantry")) {
                    isCardValid = true;
                }
            }
        }
       //9th pattern-all different card
        if(option_1.equals("artillery")) {
            if (option_2.equals("infantry")) {
                if (option_3.equals("cavalry")) {
                    isCardValid = true;
                }
            }
        }




                return isCardValid;
    }
/**
 * This method is for player world domination view
 * @param players:object of Player class
 * @param territory:object of Territory class
 * @param continent:object of Continent class
 * @param sub:object of Denomination Subject class
 */
    public void playerWorldDominationView(Players players, Territory territory, Continent continent,DenominationSub sub) {
       String armiesPercentage= players.playerDominationView(players,territory);
       System.out.println("Armies and its Percentage-\n"+ armiesPercentage);
       sub.setarmiesOwnedMsg("Armies and its Percentage-\n"+ armiesPercentage);
       sub.setMapControlledMsg("Armies and its Percentage-\n"+ armiesPercentage);
       String playerOwnedContinents= String.valueOf(players.playerOwnedContinents(players,continent));
        System.out.println("players owned continents-\n"+ playerOwnedContinents);
        sub.setcontinentsControlledMsg("players owned continents-\n"+ playerOwnedContinents);
    }
/**
 * This method provide logic for player world domination view
 * @param players:object of Player class
 * @param territory:object of Territory class
 * @return player army count
 */
    public String playerDominationView(Players players, Territory territory) {

        int totalterritoryCount;
        String playerArmyCount = "";


        for(String player : players.playersNameList) {

            int armyCount = 0;


            ArrayList<Integer> armiesList= players.assignedArmies.get(player);
//            System.out.println("Armies List: "+armiesList+"\n");

            for (int army : armiesList) {

                armyCount += army;

            }
            System.out.println(player+": "+"Total Army Count: "+armyCount+"\n");

            ArrayList<String> territoriesList= players.playersOwnedTerritories.get(player);

            int numberofTerritories = territoriesList.size();

            System.out.println("Player owned territories: "+numberofTerritories);

            int totalnumberofTerritories = Territory.totalTerritories;

            System.out.println("Total number of territories: "+totalnumberofTerritories);

            float playerOwnedTerritoryPercentage = numberofTerritories*100f /totalnumberofTerritories;

            playerArmyCount = playerArmyCount+""+player+":"+armyCount+"Player owned territory percentage: "+playerOwnedTerritoryPercentage+"\n";



        }

        return  playerArmyCount;

    }
/**
 * This method provide code for player owned continents
 * @param players:object of Player class
 * @param continent:object of Continent class
 * @return Map continents owned by player
 */
    public Map playerOwnedContinents(Players players, Continent continent) {

        Map<String,String> playerOwnedContinent = new HashMap<>();

        for (Map.Entry<String, List<String>> key: continent.continentTerritories.entrySet()) {

            for (Map.Entry<String, ArrayList<String>> keys: players.playersOwnedTerritories.entrySet()) {

                String continentName = key.getKey();
                String playerName = keys.getKey();
                List<String> continentTerritories = key.getValue();
                List<String> playerTerritories = keys.getValue();

                if (playerTerritories != null) {

                    Boolean commonTerritories = playerTerritories.containsAll(continentTerritories);

                    if (commonTerritories) {

                        playerOwnedContinent.put(continentName, playerName);

                    }
                }

            }
        }

        return playerOwnedContinent;

    }


}
