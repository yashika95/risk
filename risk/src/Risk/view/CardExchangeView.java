package Risk.view;
import Risk.observer.ExchangeObserver;
import Risk.observer.ExchangeSubject;
import Risk.observer.Subject;

/**
 *This class displays card owned by the player
 */
public class CardExchangeView extends ExchangeObserver
{
	String message;
	ExchangeSubject exchangeSubject;

	public CardExchangeView(ExchangeSubject exchangeSubject){
		this.exchangeSubject=exchangeSubject;
	}
	/**
	 * Displays card owned by the player
	 * @param o:Genralized object that accept string as an object from subject class
	 */
	@Override
	public void cardsOwnedUpdate(Object o) 
	{
		message=exchangeSubject.getcardsOwned();
		System.out.println(message);
		// TODO Auto-generated method stub
		
	}
	
	
}
