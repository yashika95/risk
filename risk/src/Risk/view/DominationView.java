package Risk.view;

import Risk.observer.DenominationObserver;
import Risk.observer.DenominationSub;
import Risk.observer.Subject;
/**
 * This class display domination of player over world
 */
public class DominationView extends DenominationObserver
{
    DenominationSub dominationsub;
    String message;
    public DominationView(DenominationSub dominationsub){
        this.dominationsub=dominationsub;
    }

    /**
	 * method to update percentage of map controlled
	 * @param o:Genralized object that accept string
	 */
    @Override
    public void MapControlledUpdate(Object o) 
    {
    	message=dominationsub.getMapControlledMsg();
    	System.out.println(message);

    }
    /**
     * method to update player
	 * @param o:Genralized object that accept string
	 */
    @Override
    public void PlayerUpdate(Object o) 
    {
    	message=dominationsub.getPlayerName();
		System.out.println(message);
    }
    /**
    * method to update continents controlled
	* @param o:Genralized object that accept string
 	*/
    @Override
    public void continentsControlledUpdate(Object o) 
    {
    	message=dominationsub.getcontinentsControlledMsg();
		System.out.println(message);
    }
    /**
     * method to update armies owned
	 * @param o:Genralized object that accept string
     */
    @Override
    public void armiesOwnedUpdate(Object o) 
    {
    	message=dominationsub.getarmiesOwnedMsg();
		System.out.println(message);
    }
}
