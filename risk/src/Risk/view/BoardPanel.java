package Risk.view;

import org.apache.commons.lang3.StringUtils;

import Risk.controller.RiskGame;
import Risk.model.ArmiesCountCalculation;
import Risk.model.Continent;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.List;
import java.util.*;
/**
 *This class provide user interface
 */
public class BoardPanel extends JFrame implements ActionListener, ItemListener {
    RiskGame riskGame= new RiskGame();
    ArmiesCountCalculation armiesCountCalculation = new ArmiesCountCalculation();
    JFrame frame1,frame2,frame3,frame4,frame5,frame6;
    String no_Of_Players;
    ArrayList<String> names = new ArrayList<>();
    JComboBox<String> continents;
    JComboBox territories;
    JComboBox adjacent_territories;
    JComboBox<String> select_continent;
    JComboBox select_territory;
    JComboBox select_adjacent_territory;
    JComboBox delete_continent;
    JComboBox delete_territory;
    JComboBox delete_adjacent_territory;
    Button start_gamebt,quit_gamebt,map_editorbt,new_mapbt,edit_mapbt,create_continentbt,create_territorybt,create_adjacent_territorybt,delete_continentbt,delete_territorybt,delete_adjacent_territorybt,save_mapbt,exitbt,save_mapbt1,tournament;
    Button bt2,bt3,bt4,bt5;
    Button submit_names,load_mapbt,create_continentNewMapbt,create_territoryNewMapbt,create_adjacent_territoryNewMapbt;
    JTextField tf1,tf2,tf3,tf4,tf_name1,tf_name2,tf_name3,tf_name4,tf_name5;
    JLabel lb1,lb2,lb3,lb4,lb5,lb6,lb7,lb8,lb9,lb10,lb11,lb12;
    JRadioButton select_map;
    String[] continentlist=new String[]{};
    List<String> territory_al=new ArrayList<>();
    ArrayList<String> continent_name_al=new ArrayList<>();
    ArrayList<Integer> control_value_al=new ArrayList<Integer>();
    Map<String, Integer> continentName=new HashMap<>();
    Map<String, java.util.List<String>> continentTerritories=new HashMap<>();
    Map<String, List<String>> adjacentTerritories= new HashMap<>();
    Continent continent= new Continent();
    String dir,dir2="";
    /**
     * Constructor of Board Panel
     */
    BoardPanel(){

        setSize(250,250);
        setTitle("RISK GAME");
        setLayout(null);
        setBackground(Color.DARK_GRAY);
        quit_gamebt=new Button("LOAD GAME");
        start_gamebt=new Button("START GAME");
        map_editorbt=new Button("MAP SELECTION");
        tournament=new Button("TOURNAMENT MODE");
        start_gamebt.setBounds(10,10,200,50);
        quit_gamebt.setBounds(10,60,200,50);
        map_editorbt.setBounds(10,110,200,50);
        tournament.setBounds(10,160,200,50);
        add(start_gamebt);
        add(quit_gamebt);
        add(map_editorbt);
        add(tournament);
        start_gamebt.addActionListener(this);
        quit_gamebt.addActionListener(this);
        map_editorbt.addActionListener(this);
        tournament.addActionListener(this);
    }

    /**
     * Main method of board panel
     * @param args:Command Line Argument
     */
    public static void main(String args[])
    {
        BoardPanel panel=new BoardPanel();
        panel.setVisible(true);
    }

    @Override
    /**
     * This method performs action when button is pressed
     * @param e:Object of action event class
     */
    public void actionPerformed(ActionEvent e)
    {
    	if(e.getSource()==tournament)
    	{
    		
    	}
        if (e.getSource() == select_map) {

            String filedata="";
            JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            jfc.setDialogTitle("Select Map");
            jfc.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Map", "map");
            jfc.addChoosableFileFilter(filter);
            int returnValue = jfc.showOpenDialog(null);
            dir=jfc.getSelectedFile().getPath();
            if (returnValue == JFileChooser.APPROVE_OPTION)
            {

                System.out.println(jfc.getSelectedFile().getPath());

                Scanner scanner = null;
                try {
                    scanner = new Scanner(new File(jfc.getSelectedFile().getPath()));
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    System.out.println(line);
                    filedata=filedata+System.lineSeparator()+line;
                }

                String continentsData= StringUtils.substringBetween(filedata, "[Continents]", "[Territories]");
                continentsData.trim();
                System.out.println(continentsData);
                String territoriesData=StringUtils.substringAfter(filedata,"[Territories]");
                System.out.println(territoriesData);


                //setContinetData(continentsData,territoriesData);

                continent.setContinentName(continentsData);
                continent.setContinentTerritories(territoriesData);
                continentTerritories=continent.getContinentTerritories();

                //setTerritoryData(territoriesData);
                for(String data:territoriesData.split("\n")){
                    System.out.println("data"+data);
                    String[] territoriesDataArray = data.split(",");
                    System.out.println("Array Data"+territoriesDataArray.toString());
                    int size= territoriesDataArray.length;
                    if(size>1){
                        System.out.println("ArrayData[0]"+territoriesDataArray[0]);
                        System.out.println("Array Data"+territoriesDataArray.toString());

                        List<String> list = new ArrayList<>();
                        this.adjacentTerritories.put(territoriesDataArray[0],list);
                        for(int i=4; i<size;i++){
                            List<String> adjacentTerritoriesList= adjacentTerritories.get(territoriesDataArray[0]);
                            System.out.println(i+ territoriesDataArray[i]);
                            adjacentTerritoriesList.add(territoriesDataArray[i]);
                            this.adjacentTerritories.put(territoriesDataArray[0], adjacentTerritoriesList);

                        }

                    }
                }
                System.out.println(adjacentTerritories);


                //return "Data read successfully";
                this.continentName=continent.continentName;


                for(Map.Entry<String, Integer> enter:continentName.entrySet()){

                    String continentname=enter.getKey();
                    System.out.println(continentname+"///////////////////////////////");
                    continents.addItem(continentname);
                    select_continent.addItem(continentname);

                }

                for(Map.Entry<String,List<String>> enter:continentTerritories.entrySet()){
                    String continentname=enter.getKey();
                    System.out.println(continentname);
                    List<String> a=new ArrayList<>();
                    a=enter.getValue();
                    System.out.println(a+"444444444444444444444444444");
                    for(int i=0;i<a.size();i++){
                        System.out.println(a.get(i));
                        territories.addItem(a.get(i));

                    }

                }

            }

        }
        if(e.getSource()==start_gamebt)
        {
            System.out.println("start game pressed");
            player_Selection();

        }
        if(e.getSource()==load_mapbt)
        {
            System.out.println("Load map button pressed");

            String setMapResult= null;
            try {
                setMapResult = riskGame.setMap();

                if(setMapResult=="Invalid format"){
                    return;

                }
                if(setMapResult=="Diconnected map-Invalid format"){
                    return;

                }
                if(setMapResult=="Map Data connected check successful"){
                    //connected-correct Map
                    System.out.println(setMapResult);
                    riskGame.assignTerritoriesRandomly();
                    riskGame.setReinforcementArmies();
                    int initialArmiesCount=riskGame.initialArmycount(armiesCountCalculation);
                    riskGame.assignInitialArmiesToTerritory(initialArmiesCount);
                    riskGame.setCardsToTerritory();
                    riskGame.StartGame();

                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }


        }
        if(e.getSource()==quit_gamebt)
        {
            System.out.println("start game pressed");

        }
        if(e.getSource()==new_mapbt)
        {
            System.out.println("new map pressed");
            newMap();
        }


        if(e.getSource()==exitbt)
        {
            frame3.getDefaultCloseOperation();

        }

        if(e.getSource()==bt2)
        {
            no_Of_Players="2";
            playerSelected(2);


        }
        if(e.getSource()==bt3)
        {
            no_Of_Players="3";
            playerSelected(3);


        }
        if(e.getSource()==bt4)
        {
            no_Of_Players="4";
            playerSelected(4);


        }
        if(e.getSource()==bt5)
        {
            no_Of_Players="5";
            playerSelected(5);


        }
        if(e.getSource()==submit_names)
        {
            /* one player logic not supported in game format.
            if(no_Of_Players.equals("1")){
                names=tf_name1.getText().toString();

                System.out.println(names);
                loadMap();

            }*/
            if(no_Of_Players.equals("2")){

                names.add(tf_name1.getText());
                names.add(tf_name2.getText());
                //names.add("Neutral Player");

                riskGame.createPlayerList(names);

                armiesCountCalculation.initialArmiesCountCalculation(2);

                riskGame.assignInitialArmyCount(names, armiesCountCalculation.getArmiesCount());

                loadMap();

            }
            if(no_Of_Players.equals("3")){

                names.add(tf_name1.getText());
                names.add(tf_name2.getText());
                names.add(tf_name3.getText());

                riskGame.createPlayerList(names);

                armiesCountCalculation.initialArmiesCountCalculation(3);

                riskGame.assignInitialArmyCount(names, armiesCountCalculation.getArmiesCount());

                loadMap();
            }

            if(no_Of_Players.equals("4")){

                names.add(tf_name1.getText());
                names.add(tf_name2.getText());
                names.add(tf_name3.getText());
                names.add(tf_name4.getText());

                riskGame.createPlayerList(names);

                armiesCountCalculation.initialArmiesCountCalculation(4);

                riskGame.assignInitialArmyCount(names, armiesCountCalculation.getArmiesCount());

                loadMap();
            }
            if(no_Of_Players.equals("5")){

                names.add(tf_name1.getText());
                names.add(tf_name2.getText());
                names.add(tf_name3.getText());
                names.add(tf_name4.getText());
                names.add(tf_name5.getText());

                riskGame.createPlayerList(names);

                armiesCountCalculation.initialArmiesCountCalculation(5);

                riskGame.assignInitialArmyCount(names, armiesCountCalculation.getArmiesCount());

                loadMap();
            }

        }
        if(e.getSource().equals(map_editorbt))
        {
            startGame();
        }
        if(e.getSource().equals(edit_mapbt))
        {
            edit_Map();
        }
        if(e.getSource().equals(quit_gamebt))
        {
            dispose();
        }
        if(e.getSource().equals(exitbt))
        {
            frame3.dispose();
        }
        if(e.getSource().equals(create_continentbt))
        {

            String continent_name=tf1.getText().toString();
            String continent_control_value=tf2.getText().toString();
            continents.addItem(continent_name);
            select_continent.addItem(continent_name);
            continent_name_al.add(continent_name);
            continentName.put(continent_name.trim(), Integer.parseInt(continent_control_value));
            System.out.println(continentName);
            System.out.println(control_value_al);
            delete_continent.addItem(continent_name);

        }

        if(e.getSource().equals(create_territorybt))

        {
            int flag=0;

            String continent_name=select_continent.getSelectedItem().toString();
            System.out.println(continent_name+"continent name:");
            String territory_name=tf4.getText().toString();
            System.out.println(territory_name);
            if(continentTerritories.size()==0){
                List<String> d=new ArrayList<>();
                d.add(territory_name);
                continentTerritories.put(continent_name,d);
                select_territory.addItem(territory_name);
                select_adjacent_territory.addItem(territory_name);
                delete_territory.addItem(territory_name);
                delete_adjacent_territory.addItem(territory_name);
            }else {
                for (Map.Entry<String, List<String>> enter : continentTerritories.entrySet()) {
                    System.out.println("inside");
                    String key = enter.getKey();
                    System.out.println("key:" + key);
                    if (key.equals(continent_name)) {
                        List<String> abc = new ArrayList<>();
                        abc = enter.getValue();
                        abc.add(territory_name);
                        System.out.println(abc+"--------------------------------999");
                        continentTerritories.put(continent_name, abc);
                        System.out.println(continentTerritories+"+++++++++++++89898");
                        select_territory.addItem(territory_name);
                        select_adjacent_territory.addItem(territory_name);
                        delete_territory.addItem(territory_name);
                        delete_adjacent_territory.addItem(territory_name);

                        flag=1;

                    }

                }
                if(flag==0){

                    System.out.println(flag+"flag value");
                    List<String> d=new ArrayList<>();
                    d.add(territory_name);
                    continentTerritories.put(continent_name,d);
                    select_territory.addItem(territory_name);
                    select_adjacent_territory.addItem(territory_name);
                    delete_territory.addItem(territory_name);
                    delete_adjacent_territory.addItem(territory_name);

                }
                else if(flag==1){
                    System.out.println(flag+"flag value");

                }
            }

        }
        if(e.getSource().equals(continents)){
            //territories.removeAllItems();
            for(Map.Entry<String, List<String>> entry:continentTerritories.entrySet()){
                String key=entry.getKey();
                if(key.equals(continents.getSelectedItem().toString())){
                    List<String> abc=new ArrayList<>();
                    abc=entry.getValue();
                    for(int i=0;i<abc.size();i++){
                        String t=abc.get(i);
                        System.out.println(t+"-----------------------------------");
                        territories.addItem(t);
                        select_territory.addItem(t);

                    }

                }

            }
        }
        if(e.getSource().equals(create_adjacent_territorybt)){
            System.out.println("fffuunnnnn");
            int flag1=0;
            String selectedTerritory=select_territory.getSelectedItem().toString();
            System.out.println(selectedTerritory);
            String selectedAdjacentTerritory=select_adjacent_territory.getSelectedItem().toString();
            System.out.println(selectedAdjacentTerritory);
            for(Map.Entry<String,List<String>> enter:adjacentTerritories.entrySet()){
                String key=enter.getKey();
                System.out.println(key+"iiiiiiiiiiii");
                if(key.equals(selectedTerritory)){
                    List<String> d=new ArrayList<>();
                    d=enter.getValue();
                    d.add(selectedAdjacentTerritory);
                    adjacentTerritories.put(selectedTerritory,d);
                    System.out.println(adjacentTerritories);
                    adjacent_territories.addItem(selectedAdjacentTerritory);
                    flag1=1;

                }

            }
            if(flag1==0){
                System.out.println("flag1"+flag1);
                List<String> d=new ArrayList<>();
                d.add(selectedAdjacentTerritory);
                adjacentTerritories.put(selectedTerritory,d);
                adjacent_territories.addItem(selectedAdjacentTerritory);


            }
            else if(flag1==1){
                System.out.println("flag1"+flag1);

            }

        }
        if(e.getSource().equals(territories)){
            adjacent_territories.removeAllItems();
            String selectedterritoryName=territories.getSelectedItem().toString();
            for(Map.Entry<String,List<String>> enter:adjacentTerritories.entrySet()){
                String key=enter.getKey();
                if(key.equals(selectedterritoryName)){
                    List<String> d=new ArrayList<>();
                    d=enter.getValue();
                    for(int i=0;i<d.size();i++){
                        adjacent_territories.addItem(d.get(i));
                    }

                }

            }


        }
        ///////////////////////new map///////////////////////
        if(e.getSource().equals(create_continentNewMapbt)){
            String continent_name=tf1.getText().toString();
            String continent_control_value=tf2.getText().toString();
            continents.addItem(continent_name);
            select_continent.addItem(continent_name);
            continent_name_al.add(continent_name);
            continentName.put(continent_name, Integer.valueOf(continent_control_value));
            System.out.println(continentName);
            System.out.println(control_value_al);
            delete_continent.addItem(continent_name);

        }

        int flag=0;
        if(e.getSource().equals(create_territoryNewMapbt)){
            String continent_name=select_continent.getSelectedItem().toString();
            System.out.println(continent_name+"continent name:");
            String territory_name=tf4.getText().toString();
            System.out.println(territory_name);
            if(continentTerritories.size()==0){
                List<String> d=new ArrayList<>();
                d.add(territory_name);
                continentTerritories.put(continent_name,d);
                select_territory.addItem(territory_name);
                select_adjacent_territory.addItem(territory_name);
                delete_territory.addItem(territory_name);
                delete_adjacent_territory.addItem(territory_name);
            }
            else {
                for (Map.Entry<String, List<String>> enter : continentTerritories.entrySet()) {
                    System.out.println("inside");
                    String key = enter.getKey();
                    System.out.println("key:" + key);
                    if (key.equals(continent_name)) {
                        List<String> abc = new ArrayList<>();
                        abc = enter.getValue();
                        abc.add(territory_name);
                        System.out.println(abc+"--------------------------------999");
                        continentTerritories.put(continent_name, abc);
                        System.out.println(continentTerritories+"+++++++++++++89898");
                        select_territory.addItem(territory_name);
                        select_adjacent_territory.addItem(territory_name);
                        delete_territory.addItem(territory_name);
                        delete_adjacent_territory.addItem(territory_name);

                        flag=1;

                    }

                }

                if(flag==0){
                    System.out.println(flag+"flag value");
                    List<String> d=new ArrayList<>();
                    d.add(territory_name);
                    continentTerritories.put(continent_name,d);
                    select_territory.addItem(territory_name);
                    select_adjacent_territory.addItem(territory_name);
                    delete_territory.addItem(territory_name);
                    delete_adjacent_territory.addItem(territory_name);

                }
                else if(flag==1){

                    System.out.println(flag+"flag value");

                }
            }

        }
        if(e.getSource().equals(create_adjacent_territoryNewMapbt)){
            System.out.println("fffuunnnnn");
            int flag1=0;
            String selectedTerritory=select_territory.getSelectedItem().toString();
            System.out.println(selectedTerritory);
            String selectedAdjacentTerritory=select_adjacent_territory.getSelectedItem().toString();
            System.out.println(selectedAdjacentTerritory);
            for(Map.Entry<String,List<String>> enter:adjacentTerritories.entrySet()){
                String key=enter.getKey();
                System.out.println(key+"iiiiiiiiiiii");
                if(key.equals(selectedTerritory)){
                    List<String> d=new ArrayList<>();
                    d=enter.getValue();
                    d.add(selectedAdjacentTerritory);
                    adjacentTerritories.put(selectedTerritory,d);
                    System.out.println(adjacentTerritories);
                    adjacent_territories.addItem(selectedAdjacentTerritory);
                    flag1=1;

                }

            }
            if(flag1==0){
                System.out.println("flag1"+flag1);
                List<String> d=new ArrayList<>();
                d.add(selectedAdjacentTerritory);
                adjacentTerritories.put(selectedTerritory,d);
                adjacent_territories.addItem(selectedAdjacentTerritory);


            }
            else if(flag1==1){
                System.out.println("flag1"+flag1);

            }

        }
        if(e.getSource().equals(save_mapbt1))
        {
            //int t=0;
            //File dir1;
            String fileName ="";
            try {
                Random randomnumber = new Random();

                //System.out.println(dir);
                //dir1=new File(dir);
                //fileName=dir1.getName();
                //if(fileName.equals(""))
                //{
                fileName="mapFile"+randomnumber.nextInt(100)+".map";
                //}
                System.out.println(fileName);
                String mapFile ="maps/"+fileName;
                String Mapdata= "[Map]\n" +
                        "author=abc\n" +
                        "warn=y\n" +
                        "image=pqr.bmp\n" +
                        "wrap=n\n";
                StringBuilder continentStringBuilder = new StringBuilder();
                StringBuilder territoryStringBuilder = new StringBuilder();
                continentStringBuilder.append("[Continents]\n");
                territoryStringBuilder.append("[Territories]\n");
                File Mfile = new File(mapFile);
                //if (Mfile.exists())
                //{

                //Mfile.delete();
                //System.out.println("Existing file deleted ");
                //}
                boolean filecreation = Mfile.createNewFile();
                if (filecreation)
                {
                    System.out.println("");

                    FileWriter fw = new FileWriter(Mfile);
                    BufferedWriter bw = new BufferedWriter(fw);

                    String continentname = "";
                    String territoryname = "";
                    for (Map.Entry<String, Integer> entry : continentName.entrySet())
                    {
                        continentStringBuilder.append(entry.getKey() + "=" + entry.getValue() + "\n");

                    }

                    for (Map.Entry<String, List<String>> entry :adjacentTerritories.entrySet())
                    {
                        String key= entry.getKey();
                        territoryname=key;
                        List<String> adjacent_territories = entry.getValue();


                        for (Map.Entry<String, List<String>> entry1 : continentTerritories.entrySet())
                        {
                            System.out.println("-------------------------------------------");
                            List<String> adjacent_territoriesAl = new ArrayList<>();
                            adjacent_territoriesAl = entry1.getValue();
                            for (int i = 0; i < adjacent_territoriesAl.size() ; i++) {
                                if (key.equals(adjacent_territoriesAl.get(i))) {
                                    continentname = entry1.getKey();

                                }

                            }

                        }
                        territoryStringBuilder.append(territoryname + ",0,0," + continentname);
                        for (int i = 0; i < adjacent_territories.size() ; i++) {
                            territoryStringBuilder.append("," + adjacent_territories.get(i));
                            System.out.println(adjacent_territories.get(i));
                        }
                        territoryStringBuilder.append("\n");

                    }
                    System.out.println(Mapdata+ "" + continentStringBuilder + "" + territoryStringBuilder);
                    bw.write(Mapdata+ "" + continentStringBuilder + "" + territoryStringBuilder);
                    bw.flush();
                    bw.close();
                    System.out.println("File Creation successful");
                }

            }
            catch (Exception e2)
            {
                System.out.println("file creation failed");
                e2.printStackTrace();
            }
        }
        if(e.getSource().equals(save_mapbt))
        {
            int t=0;
            File dir1;
            String fileName ="";
            try {
                Random randomnumber = new Random();

                System.out.println(dir);
                dir1=new File(dir);
                fileName=dir1.getName();
                if(fileName.equals(""))
                {
                    fileName="maps/mapFile"+randomnumber.nextInt(100)+".map";
                }
                System.out.println(fileName);
                //  String mapFile ="maps/"+fileName;
                String Mapdata= "[Map]\n" +
                        "author=abc\n" +
                        "warn=y\n" +
                        "image=pqr.bmp\n" +
                        "wrap=n\n";
                StringBuilder continentStringBuilder = new StringBuilder();
                StringBuilder territoryStringBuilder = new StringBuilder();
                continentStringBuilder.append("[Continents]\n");
                territoryStringBuilder.append("[Territories]\n");
                File Mfile = new File(dir);
                if (Mfile.exists())
                {
                    if(Mfile.delete())
                        System.out.println("Existing file deleted ");
                }
                boolean filecreation = Mfile.createNewFile();
                if (filecreation)
                {
                    System.out.println("");

                    FileWriter fw = new FileWriter(Mfile, false);
                    //  BufferedWriter bw = new BufferedWriter(fw);

                    String continentname = "";
                    String territoryname = "";
                    for (Map.Entry<String, Integer> entry : continentName.entrySet())
                    {
                        continentStringBuilder.append(entry.getKey() + "=" + entry.getValue() + "\n");
                        //bw.write(entry.getKey() + "=" + entry.getValue() + "\n");
                    }

                    for (Map.Entry<String, List<String>> entry :adjacentTerritories.entrySet())
                    {
                        String key= entry.getKey();
                        territoryname=key;
                        List<String> adjacent_territories = entry.getValue();


                        for (Map.Entry<String, List<String>> entry1 : continentTerritories.entrySet())
                        {
                            System.out.println("-------------------------------------------");
                            List<String> adjacent_territoriesAl = new ArrayList<>();
                            adjacent_territoriesAl = entry1.getValue();
                            for (int i = 0; i < adjacent_territoriesAl.size() ; i++) {
                                if (key.equals(adjacent_territoriesAl.get(i))) {
                                    continentname = entry1.getKey();

                                }

                            }

                        }
                        territoryStringBuilder.append(territoryname + ",0,0," + continentname);
                        for (int i = 0; i < adjacent_territories.size() ; i++) {
                            territoryStringBuilder.append("," + adjacent_territories.get(i));
                            System.out.println(adjacent_territories.get(i));
                        }
                        territoryStringBuilder.append("\n");

                    }
                    System.out.println(Mapdata+ "" + continentStringBuilder + "" + territoryStringBuilder);
                    fw.write(Mapdata+ "" + continentStringBuilder + "" + territoryStringBuilder);
                    //bw.flush();
                    //bw.close();
                    fw.close();
                    System.out.println("File Creation successful");
                }

            }
            catch (Exception e2)
            {
                System.out.println("file creation failed");
                e2.printStackTrace();
            }
        }if(e.getSource().equals(delete_continentbt)){
        String continent_tobe_deleted=delete_continent.getSelectedItem().toString();
        if(continentName.containsKey(continent_tobe_deleted)){
            continentName.remove(continent_tobe_deleted);
            continents.removeItem(continent_tobe_deleted);
            select_continent.removeItem(continent_tobe_deleted);
            System.out.println(continent_tobe_deleted+"removed");
            System.out.println(continentName);
        }

        List<String> dummy=new ArrayList<>();
        if(continentTerritories.containsKey(continent_tobe_deleted)){
            dummy=continentTerritories.get(continent_tobe_deleted);
            continentTerritories.remove(continent_tobe_deleted);
            System.out.println(continent_tobe_deleted+"removed");
            for(int i=0;i<dummy.size();i++) {
                String keys=dummy.get(i);
                adjacentTerritories.remove(keys);
                select_territory.removeItem(keys);
                select_adjacent_territory.removeItem(keys);
                adjacent_territories.removeItem(keys);

            }

        }

    }
        if(e.getSource().equals(delete_territory)){
            delete_adjacent_territory.removeAllItems();
            String selectedterritoryName=delete_territory.getSelectedItem().toString();
            for(Map.Entry<String,List<String>> enter:adjacentTerritories.entrySet()){
                String key=enter.getKey();
                if(key.equals(selectedterritoryName)){
                    List<String> d=new ArrayList<>();
                    d=enter.getValue();
                    for(int i=0;i<d.size();i++){
                        delete_adjacent_territory.addItem(d.get(i));
                    }

                }

            }

        }
        if(e.getSource().equals(delete_territorybt)){
            String continentname="";
            String territory_to_bedeleted=delete_territory.getSelectedItem().toString();
            System.out.println(territory_to_bedeleted+adjacentTerritories+select_territory);
            adjacentTerritories.remove(territory_to_bedeleted);
            select_territory.removeItem(territory_to_bedeleted);

            for(Map.Entry<String,List<String>> entry:continentTerritories.entrySet()){
                List<String> dummy=new ArrayList<>();
                dummy=entry.getValue();
                for(int i=0;i<dummy.size();i++){
                    if(dummy.get(i).equals(territory_to_bedeleted)){
                        continentname=entry.getKey();
                    }
                }

            }

            //continentTerritories.remove(continentName);

        }if(e.getSource().equals(delete_adjacent_territorybt)){
        String selected_adjacent_territory=delete_adjacent_territory.getSelectedItem().toString();
        adjacentTerritories.remove(selected_adjacent_territory);

    }

    }
    /**
     * This method provides user interface to start the game
     */
    public void startGame(){
        frame2=new JFrame("hello");
        frame2.setSize(250,250);
        frame2.setVisible(true);
        frame2.setLayout(null);
        new_mapbt=new Button("CREATE NEW MAP");
        edit_mapbt=new Button("EDIT MAP");
        new_mapbt.setBounds(10,10,200,50);
        edit_mapbt.setBounds(10,90,200,50);
        frame2.add(edit_mapbt);
        frame2.add(new_mapbt);
        new_mapbt.addActionListener(this);
        edit_mapbt.addActionListener(this);

    }
    /**
     * This method provides user interface to generate new map
     */
    public void newMap(){
        frame3=new JFrame("NEW MAP");
        frame3.setSize(2000,2000);
        frame3.setLayout(null);
        frame3.setVisible(true);

        continents=new JComboBox<>();
        continents.setBounds(10,50,200,30);
        continents.setVisible(true);
        continents.setEditable(true);
        territories=new JComboBox();
        territories.setBounds(230,50,200,30);
        territories.setVisible(true);
        territories.setEditable(true);

        adjacent_territories=new JComboBox();
        adjacent_territories.setBounds(450,50,200,30);
        adjacent_territories.setVisible(true);
        adjacent_territories.setEditable(true);

        lb1=new JLabel("Create a new Map");
        lb1.setBounds(10,10,200,150);
        lb2=new JLabel("TERRITORIES");
        lb2.setBounds(230,10,100,50);
        lb3=new JLabel("ADJACENT TERRITORIES");
        lb3.setBounds(450,10,200,50);
        lb4=new JLabel("add a continent name");
        lb4.setBounds(10,90,100,50);
        lb5=new JLabel("continent name");
        lb5.setBounds(10,180,100,50);
        lb5.setVisible(true);
        tf1=new JTextField("");
        tf1.setBounds(120,180,200,30);
        tf1.setBackground(Color.WHITE);
        tf1.setEditable(true);

        lb6=new JLabel("control value");
        lb6.setBounds(520,180,100,50);


        tf2=new JTextField("");
        tf2.setBounds(720,180,200,30);
        tf2.setBackground(Color.WHITE);
        tf2.setEditable(true);
        create_continentNewMapbt=new Button("CREATE CONTINENT");
        create_continentNewMapbt.setBounds(1020,180,150,20);
        create_continentNewMapbt.addActionListener(this);
        ////////////////////////////////////////////////
        lb7=new JLabel("add a territory");
        lb7.setBounds(10,130,100,50);
        lb8=new JLabel("continent name");
        lb8.setBounds(10,280,100,50);
        lb8.setVisible(true);
        select_continent=new JComboBox<>();
        select_continent.setBounds(220,280,200,30);
        select_continent.setEditable(true);
        lb9=new JLabel("territory name");
        lb9.setBounds(570,280,100,50);


        tf4=new JTextField("");
        tf4.setBounds(720,280,200,30);
        tf4.setBackground(Color.WHITE);
        tf4.setEditable(true);
        create_territoryNewMapbt=new Button("CREATE territory");
        create_territoryNewMapbt.setBounds(1020,280,150,20);
        create_territoryNewMapbt.addActionListener(this);
        ////////////////////////////////////
        lb10=new JLabel("add adjacent territory");
        lb10.setBounds(10,350,200,50);
        lb11=new JLabel("territory");
        lb11.setBounds(10,380,100,50);
        lb11.setVisible(true);
        select_territory=new JComboBox();
        select_territory.setBounds(220,380,200,30);
        select_territory.setEditable(true);
        lb12=new JLabel(" it's  adjacent territory ");
        lb12.setBounds(570,380,200,50);
        select_adjacent_territory=new JComboBox();
        select_adjacent_territory.setBounds(780,380,200,30);
        select_adjacent_territory.setEditable(true);



        create_adjacent_territoryNewMapbt=new Button("create adjacent territory");
        create_adjacent_territoryNewMapbt.setBounds(1050,380,150,20);
        create_adjacent_territoryNewMapbt.addActionListener(this);
        ////////////////////////////////////////////////
        delete_continent=new JComboBox();
        delete_continent.setBounds(10,480,150,30);
        delete_continentbt=new Button("delete continent");
        delete_continentbt.setBounds(180,480,150,30);
        delete_continentbt.addActionListener(this);

        delete_territory=new JComboBox();
        delete_territory.addActionListener(this);
        delete_territory.setBounds(360,480,150,30);
        delete_territorybt=new Button("delete territory");
        delete_territorybt.setBounds(530,480,150,30);
        delete_territorybt.addActionListener(this);

        delete_adjacent_territory=new JComboBox();
        delete_adjacent_territory.setBounds(750,480,150,30);
        delete_adjacent_territorybt=new Button("delete  adjacent territory");
        delete_adjacent_territorybt.setBounds(950,480,150,30);
        delete_adjacent_territorybt.addActionListener(this);
        /////////////////////////////////////////
        save_mapbt1=new Button("save");
        save_mapbt1.setBounds(500,550,150,30);
        save_mapbt1.addActionListener(this);
        exitbt=new Button("exit");
        exitbt.setBounds(750,550,150,30);
        exitbt.addActionListener(this);

        frame3.add(lb1);
        /* frame3.add(lb2);
         frame3.add(lb3);*/
//         frame3.add(lb4);
        frame3.add(lb5);
        frame3.add(lb6);
//         frame3.add(lb7);
        frame3.add(lb8);
        frame3.add(lb9);
        frame3.add(lb10);
        frame3.add(lb11);
        frame3.add(lb12);
        frame3.add(tf1);
        frame3.add(tf2);
        // frame3.add(tf3);
        frame3.add(tf4);

        frame3.add(create_continentNewMapbt);
        frame3.add(create_territoryNewMapbt);
        frame3.add(create_adjacent_territoryNewMapbt);
        frame3.add(delete_continentbt);
        frame3.add(delete_territorybt);
        frame3.add(delete_adjacent_territorybt);
        frame3.add(save_mapbt1);
        frame3.add(exitbt);
         /*frame3.add(continents);
         frame3.add(territories);
         frame3.add(adjacent_territories);*/
        frame3.add(select_continent);
        frame3.add(select_territory);
        frame3.add(select_adjacent_territory);
        frame3.add(delete_continent);
        frame3.add(delete_territory);
        frame3.add(delete_adjacent_territory);
    }
    /**
     * This method provides user interface to select the player
     */
    public void player_Selection(){
        frame4= new JFrame("PLAYER SELECTION");
        frame4.setLayout(null);
        frame4.setVisible(true);
        frame4.setSize(500,500);
        /////////////////////////////////

        bt2= new Button("TWO");
        bt2.setBounds(10,60,150,30);
        bt2.addActionListener(this);

        bt3= new Button("THREE");
        bt3.setBounds(10,110,150,30);
        bt3.addActionListener(this);

        bt4= new Button("FOUR");
        bt4.setBounds(10,160,150,30);
        bt4.addActionListener(this);

        bt5= new Button("FIVE");
        bt5.setBounds(10,210,150,30);
        bt5.addActionListener(this);


        frame4.add(bt2);
        frame4.add(bt3);
        frame4.add(bt4);
        frame4.add(bt5);




    }
    /**
     * This method provides user interface to edit the map
     */
    public void edit_Map()
    {
        select_map=new JRadioButton("open a map to edit");
        select_map.setBounds(10,2,100,15);
        select_map.addActionListener(this);
        select_map.setVisible(true);



        ////////////////////
        frame3=new JFrame("EDIT MAP");
        frame3.setSize(2000,2000);
        frame3.setLayout(null);
        frame3.setVisible(true);
        continents=new JComboBox<>();
        continents.setBounds(10,50,200,30);
        continents.setVisible(true);
        continents.setEditable(true);
        continents.addActionListener(this);

        territories=new JComboBox<>();
        territories.setBounds(230,50,200,30);
        territories.setVisible(true);
        territories.setEditable(true);
        territories.addActionListener(this);


        adjacent_territories=new JComboBox();
        adjacent_territories.setBounds(450,50,200,30);
        adjacent_territories.setVisible(true);
        adjacent_territories.setEditable(true);
        adjacent_territories.addActionListener(this);


        lb1=new JLabel("CONTINENTS");
        lb1.setBounds(10,10,100,50);
        lb2=new JLabel("TERRITORIES");
        lb2.setBounds(230,10,100,50);
        lb3=new JLabel("ADJACENT TERRITORIES");
        lb3.setBounds(450,10,200,50);
        lb4=new JLabel("add a continent");
        lb4.setBounds(10,80,100,50);
        lb5=new JLabel("continent name");
        lb5.setBounds(10,180,100,50);
        lb5.setVisible(true);
        tf1=new JTextField("");
        tf1.setBounds(120,180,200,30);
        tf1.setBackground(Color.WHITE);
        tf1.setEditable(true);
        lb6=new JLabel("control value");
        lb6.setBounds(520,180,100,50);


        tf2=new JTextField("");
        tf2.setBounds(720,180,200,30);
        tf2.setBackground(Color.WHITE);
        tf2.setEditable(true);
        create_continentbt=new Button("CREATE CONTINENT");
        create_continentbt.setBounds(1020,180,150,20);
        create_continentbt.addActionListener(this);
        ////////////////////////////////////////////////
        lb7=new JLabel("add a territory");
        lb7.setBounds(10,130,100,50);
        lb8=new JLabel("continent name");
        lb8.setBounds(10,280,100,50);
        lb8.setVisible(true);
        select_continent=new JComboBox<String>();
        select_continent.setBounds(220,280,200,30);
        select_continent.setEditable(true);


        lb9=new JLabel("territory name");
        lb9.setBounds(570,280,100,50);


        tf4=new JTextField("");
        tf4.setBounds(720,280,200,30);
        tf4.setBackground(Color.WHITE);
        tf4.setEditable(true);
        create_territorybt=new Button("CREATE territory");
        create_territorybt.setBounds(1020,280,150,20);
        create_territorybt.addActionListener(this);
        ////////////////////////////////////
        lb10=new JLabel("add adjacent territory");
        lb10.setBounds(10,350,200,50);
        lb11=new JLabel("territory");
        lb11.setBounds(10,380,100,50);
        lb11.setVisible(true);
        select_territory=new JComboBox();
        select_territory.setBounds(220,380,200,30);
        select_territory.setEditable(true);
        select_territory.addActionListener(this);

        lb12=new JLabel(" it's  adjacent territory ");
        lb12.setBounds(570,380,200,50);
        select_adjacent_territory=new JComboBox();
        select_adjacent_territory.setBounds(780,380,200,30);
        select_adjacent_territory.setEditable(true);



        create_adjacent_territorybt=new Button("create adjacent territory");
        create_adjacent_territorybt.setBounds(1050,380,150,20);
        create_adjacent_territorybt.addActionListener(this);
        ////////////////////////////////////////////////
        delete_continent=new JComboBox();
        delete_continent.setBounds(10,480,150,30);
        delete_continentbt=new Button("delete continent");
        delete_continentbt.setBounds(180,480,150,30);

        delete_territory=new JComboBox();
        delete_territory.setBounds(360,480,150,30);
        delete_territorybt=new Button("delete territory");
        delete_territorybt.setBounds(530,480,150,30);

        delete_adjacent_territory=new JComboBox();
        delete_adjacent_territory.setBounds(750,480,150,30);
        delete_adjacent_territorybt=new Button("delete  adjacent territory");
        delete_adjacent_territorybt.setBounds(950,480,150,30);
        /////////////////////////////////////////
        save_mapbt=new Button("save");
        save_mapbt.setBounds(500,550,150,30);
        save_mapbt.addActionListener(this);
        exitbt=new Button("exit");
        exitbt.setBounds(750,550,150,30);
        exitbt.addActionListener(this);








        frame3.add(lb1);
        frame3.add(lb2);
        frame3.add(lb3);
        frame3.add(lb4);
        frame3.add(lb5);
        frame3.add(lb6);
        frame3.add(lb7);
        frame3.add(lb8);
        frame3.add(lb9);
        frame3.add(lb10);
        frame3.add(lb11);
        frame3.add(lb12);
        frame3.add(tf1);
        frame3.add(tf2);
        // frame3.add(tf3);
        frame3.add(tf4);

        frame3.add(create_continentbt);
        frame3.add(create_territorybt);
        frame3.add(create_adjacent_territorybt);
        frame3.add(delete_continentbt);
        frame3.add(delete_territorybt);
        frame3.add(delete_adjacent_territorybt);
        frame3.add(save_mapbt);
        frame3.add(exitbt);
        frame3.add(continents);
        frame3.add(territories);
        frame3.add(adjacent_territories);
        frame3.add(select_continent);
        frame3.add(select_territory);
        frame3.add(select_adjacent_territory);
        frame3.add(delete_continent);
        frame3.add(delete_territory);
        frame3.add(delete_adjacent_territory);
        frame3.add(select_map);

        //////////////////
    }
    /**
     * This method provides user interface to select the player
     * @param n:no. of players
     */
    public void playerSelected(int n){
        frame5=new JFrame();
        frame5.setSize(300,300);
        frame5.setVisible(true);
        frame5.setLayout(null);

        if(n==1){
            tf_name1=new JTextField();
            tf_name1.enableInputMethods(true);
            tf_name1.setEditable(true);
            tf_name1.setBounds(10,10,100,30);


            System.out.println(n);
            submit_names=new Button("SUBMIT");
            submit_names.setBounds(50,100,100,30);
            submit_names.addActionListener(this);
            frame5.add(submit_names);
            frame5.add(tf_name1);
        }
        if(n==2){

            tf_name1=new JTextField();
            tf_name1.enableInputMethods(true);
            tf_name1.setEditable(true);

            tf_name1.setBounds(10,10,100,30);
            tf_name2=new JTextField();
            tf_name2.enableInputMethods(true);
            tf_name2.setEditable(true);
            tf_name2.setBounds(10,45,100,30);
            submit_names=new Button("SUBMIT");
            submit_names.setBounds(50,100,100,30);
            submit_names.addActionListener(this);


            System.out.println(names);
            frame5.add(tf_name1);
            frame5.add(tf_name2);
            frame5.add(submit_names);
        }

        if(n==3){
            tf_name1=new JTextField();
            tf_name1.enableInputMethods(true);
            tf_name1.setEditable(true);
            tf_name1.setBounds(10,10,100,30);

            tf_name2=new JTextField();
            tf_name2.enableInputMethods(true);
            tf_name2.setEditable(true);

            tf_name2.setBounds(10,50,100,30);

            tf_name3=new JTextField();
            tf_name3.enableInputMethods(true);
            tf_name3.setEditable(true);

            tf_name3.setBounds(10,85,100,30);

            System.out.println(names);
            submit_names=new Button("SUBMIT");
            submit_names.setBounds(50,120,100,30);
            submit_names.addActionListener(this);

            frame5.add(tf_name1);
            frame5.add(tf_name2);
            frame5.add(tf_name3);
            frame5.add(submit_names);
        }
        if(n==4){
            tf_name1=new JTextField();
            tf_name1.enableInputMethods(true);
            tf_name1.setEditable(true);
            tf_name1.setBounds(10,10,100,30);

            tf_name2=new JTextField();
            tf_name2.enableInputMethods(true);
            tf_name2.setEditable(true);
            tf_name2.setBounds(10,45,100,30);

            tf_name3=new JTextField();
            tf_name3.enableInputMethods(true);
            tf_name3.setEditable(true);
            tf_name3.setBounds(10,85,100,30);

            tf_name4=new JTextField();
            tf_name4.enableInputMethods(true);
            tf_name4.setEditable(true);
            tf_name4.setBounds(10,125,100,30);
            //names=tf_name1.getText().toString()+","+tf_name2.getText().toString()+","+tf_name3.getText().toString()+","+tf_name4.getText().toString();

            submit_names=new Button("SUBMIT");
            submit_names.setBounds(50,160,100,30);
            submit_names.addActionListener(this);
            System.out.println(names);

            frame5.add(tf_name1);
            frame5.add(tf_name2);
            frame5.add(tf_name3);
            frame5.add(tf_name4);
            frame5.add(submit_names);


        }
        if(n==5){
            tf_name1=new JTextField();
            tf_name1.enableInputMethods(true);
            tf_name1.setEditable(true);
            tf_name1.setBounds(10,45,100,30);

            tf_name2=new JTextField();
            tf_name2.enableInputMethods(true);
            tf_name2.setEditable(true);
            tf_name2.setBounds(10,85,100,30);

            tf_name3=new JTextField();
            tf_name3.enableInputMethods(true);
            tf_name3.setEditable(true);
            tf_name3.setBounds(10,115,100,30);

            tf_name4=new JTextField();
            tf_name4.enableInputMethods(true);
            tf_name4.setEditable(true);
            tf_name4.setBounds(10,150,100,30);

            tf_name5=new JTextField();
            tf_name5.enableInputMethods(true);
            tf_name5.setEditable(true);
            tf_name5.setBounds(10,185,100,30);
            System.out.println(names);
            submit_names=new Button("SUBMIT");
            submit_names.setBounds(50,230,100,30);
            submit_names.addActionListener(this);
            System.out.println(names);

            frame5.add(tf_name1);
            frame5.add(tf_name2);
            frame5.add(tf_name3);
            frame5.add(tf_name4);
            frame5.add(tf_name5);
            frame5.add(submit_names);


        }


    }
    /**
     * This method provides user interface to load the map
     */
    public void loadMap(){
        frame6 =new JFrame("load map");
        frame6.setLayout(null);
        frame6.setSize(250,250);
        frame6.setVisible(true);
        load_mapbt= new Button("LOAD MAP");
        load_mapbt.setBounds(20,10,150,30);
        load_mapbt.addActionListener(this);
        frame6.add(load_mapbt);

    }

    @Override
    public void itemStateChanged(ItemEvent e) {



    }
}