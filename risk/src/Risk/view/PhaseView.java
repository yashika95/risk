package Risk.view;

import Risk.observer.Observer;
import Risk.observer.Subject;
/**
 * This class provide update of each phase
 */
public class PhaseView extends Observer
{
	String message;
	Subject subject;
	public PhaseView(Subject subject){
		this.subject=subject;
	}
	/**
	 * method to update the phase
	 *@param o:Genralized object that accept string
	 */
	@Override
	public void PhaseUpdate(Object o) 
	{
		
		message=subject.getPhase();
		System.out.println(message);
	}
	/**
	 * method to update the Fortification Phase
	 * @param o:Genralized object that accept string
	 */
	@Override
	public void FortificationUpdate(Object o) 
	{
		// TODO Auto-generated method stub
		message=subject.getfortificationMsg();
		System.out.println(message);
	}
	/**
	 * method to update the attack Phase
	 * @param o:Genralized object that accept string
	 */
	@Override
	public void AttackUpdate(Object o) 
	{
		// TODO Auto-generated method stub
		message=subject.getattackMsg();
		System.out.println(message);
	}
	/**
	 * method to update the Reinforcement Phase
	 * @param o:Genralized object that accept string
	 */
	@Override
	public void ReinforcementUpdate(Object o) 
	{
		// TODO Auto-generated method stub
		message=subject.getreinforcementMsg();
		System.out.println(message);
		
	}
	/**
	 * method to provide player update
	 * @param o:Genralized object that accept string
	 */
	@Override
	public void PlayerUpdate(Object o) 
	{
		// TODO Auto-generated method stub
		message=subject.getPlayerName();
		System.out.println(message);
		
	}

}
