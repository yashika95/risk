package Risk.observer;

import java.util.ArrayList;
import java.util.List;

/**
 *This class is concrete subject class
 */
public class ExchangeSubject 
{
	List<ExchangeObserver> observers = new ArrayList<>();
	private String CardsOwned;
	/**
	 * This method attaches observers
	 * @param observer:Object of ExchangeObserver
	 */
	public void attach(ExchangeObserver observer)
	{
		observers.add(observer);
	}
	/**
	 * This method detaches observers
	 * @param observer:Object of ExchangeObserver
	 */
	public void deattach(ExchangeObserver observer)
	{
		observers.remove(observer);
	}
	/**
	 * This method is used to get cards owned by the player
	 * @return cards owned
	 */
	public String getcardsOwned()
	{
		return CardsOwned;
		
	}
	/**
	 * This method is used to set cards owned by the player
	 * @param CardsOwned:cards owned by the player
	 */
	public void setcardsOwned(String CardsOwned)
	{
		this.CardsOwned=CardsOwned;
		notifyCardsOwned();
	}
	/**
	 * Notify all observers
	 */
	public void notifyCardsOwned()
	{
		for (ExchangeObserver obs : this.observers) 
		{
           obs.cardsOwnedUpdate(this.CardsOwned);
        }
	}
}
