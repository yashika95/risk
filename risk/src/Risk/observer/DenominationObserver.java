package Risk.observer;
/**
 *This is an abstract class which is extended by DenominationView class as an observer
 */
public abstract class DenominationObserver 
{
	/**
	 * abstract method to update percentage of map controlled
	 * @param o:Genralized object that accept string
	 */
	public abstract void MapControlledUpdate(Object o);
	/**
	 * abstract method to update player
	 * @param o:Genralized object that accept string
	 */
	public abstract void PlayerUpdate(Object o);
	/**
	 * abstract method to update continents controlled
	 * @param o:Genralized object that accept string
	 */
	public abstract void continentsControlledUpdate(Object o);
	/**
	 * abstract method to update armies owned
	 * @param o:Genralized object that accept string
	 */
	public abstract void armiesOwnedUpdate(Object o);
}
