package Risk.observer;

import java.util.ArrayList;
import java.util.List;
/** 
 * This is the concerete subject class
 */
public class DenominationSub 
{
	
	List<DenominationObserver> observers = new ArrayList<>();
	private String playerName;
	private String mapControlledmsg;
	private String continentsControlled;
	private String armiesOwned;
	/**
	 * This method attaches observers
	 * @param ob:Object of DenominationObserver class 
	 */
	public void attach(DenominationObserver ob) 
	{
		observers.add(ob);
	}
	/**
	 * This method detaches observers
	 * @param ob:Object of DenominationObserver class
	 */
	public void deattach(DenominationObserver ob)
	{
		observers.remove(ob);
	}
	/**
	 * This method is used to get Map Controlled Message
	 * @return map controlled message
	 */
	public String getMapControlledMsg()
	{
		return mapControlledmsg;
		
	}
	/**
	 * This method is used to set Map Controlled Message
	 * @param mapControlledmsg: message for map controlled
	 */
	public void setMapControlledMsg(String mapControlledmsg)
	{
		this.mapControlledmsg=mapControlledmsg;
		notifyMapControlled();
		
	}
	/**
	 * Notify all observers
	 */
	public void notifyMapControlled()
	{
		for (DenominationObserver ob : this.observers) 
		{
           ob.MapControlledUpdate(this.mapControlledmsg);
        }
		
	}
	/**
	 * This method is used to get Player Name
	 * @return name of the player
	 */
	public String getPlayerName()
	{
		return playerName;
	}
	/**
	 * This method is used to set Player Name
	 * @param playerName:name of player
	 */
	public void setPlayerName(String playerName)
	{
		this.playerName = playerName;
		notifyPlayerName();
	}
	/**
	 * Notify all observers
	 */
	public void notifyPlayerName()
	{
		for (DenominationObserver ob : this.observers) 
		{
           ob.PlayerUpdate(this.playerName);
        }
	}
	/**
	 * This method is used to get continents controlled message
	 * @return Continents Controlled by the player
	 */
	public String getcontinentsControlledMsg()
	{
		
		return continentsControlled;
	}
	/**
	 * This method is used to get continents controlled message
	 * @param continentsControlled:continents controlled message
	 */
	public void setcontinentsControlledMsg(String continentsControlled)
	{
		this.continentsControlled=continentsControlled;
		notifyContinentsControlled();
	}
	/**
	 * Notify all observers
	 */
	public void notifyContinentsControlled()
	{
		for (DenominationObserver ob : this.observers) 
		{
           ob.continentsControlledUpdate(this.continentsControlled);
        }
	}
	/**
	 * This method is used to get armies owned message
	 * @return armies owned by the player
	 */
	public String getarmiesOwnedMsg()
	{
		return armiesOwned;
		
	}
	/**
	 * This method is used to set armies owned message
	 * @param armiesOwned:armies owned message
	 */
	public void setarmiesOwnedMsg(String armiesOwned)
	{
		this.armiesOwned=armiesOwned;
		notifyArmiesOwned();
	}
	/**
	 * Notify all observers
	 */
	public void notifyArmiesOwned()
	{
		for (DenominationObserver ob : this.observers) 
		{
			ob.armiesOwnedUpdate(this.armiesOwned);
        }
	}
}
