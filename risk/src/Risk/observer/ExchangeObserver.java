package Risk.observer;
/**
 *This is an abstract class which is extended by ExchangeView class as an observer
 */
public abstract class ExchangeObserver 
{
	/**
	 * This method displays cards owned by the current player
	 * @param o:Genralized object that accept string
	 */
	public abstract void cardsOwnedUpdate(Object o);
}
