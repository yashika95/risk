package Risk.observer;
/**
 *This is an abstract class which is extended by PhaseView class as an observer
 */
public abstract class Observer
{
	/**
	 * abstract method to update the phase
	 * @param o:Genralized object that accept string
	 */
	public abstract void PhaseUpdate(Object o);
	/**
	 * abstract method to update the Fortification Phase
	 * @param o:Genralized object that accept string
	 */
	public abstract void FortificationUpdate(Object o);
	/**
	 *abstract method to update the attack Phase
	 * @param o:Genralized object that accept string
	 */
	public abstract void AttackUpdate(Object o);
	/**
	 * abstract method to update the Reinforcement Phase
	 * @param o:Genralized object that accept string
	 */
	public abstract void ReinforcementUpdate(Object o);
	/**
	 * abstract method to provide player update
	 * @param o:Genralized object that accept string
	 */
	public abstract void PlayerUpdate(Object o);

}
