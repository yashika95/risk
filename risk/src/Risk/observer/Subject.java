package Risk.observer;
import java.util.ArrayList;
import java.util.List;
/**
 *This class is concrete subject class
 */
public class Subject 
{
	List<Observer> observers = new ArrayList<>();
	private String fortificationMsg;
	private String reinforcementMsg;
	private String attackMsg;
	private String phase;
	private String playerName;
	/**
	 * This method attaches observers
	 * @param observer:Object of Observer 
	 */
	public void attach(Observer observer) 
	{
		observers.add(observer);
	}
	/**
	 * This method detaches observers
	 * @param observer:Object of Observer 
	 */
	public void deattach(Observer observer)
	{
		observers.remove(observer);
	}

	/**
	 * This method is used to get Phase
	 * @return phase
	 */
	public String getPhase() 
	{
		return phase;
	}
	/**
	 * This method is used to set the phase
	 * @param phase:information of phase
	 */
	public void setPhase(String phase)
	{
		this.phase=phase;
		notifyPhase();
	}
	/**
	 * Notify all observers
	 */
	public void notifyPhase()
	{
		for (Observer observer : this.observers) 
		{
           observer.PhaseUpdate(this.phase);
        }
	}
	/**
	 * This method is used to get fortification message 
	 * @return fortification message
	 */
	public String getfortificationMsg()
	{
		return fortificationMsg;
	}
	/**
	 * This method is used to set fortification message
	 * @param fortificationMsg:fortification message 
	 */
	public void setfortificationMsg(String fortificationMsg) 
	{
		this.fortificationMsg = fortificationMsg;
		notifyFortification();
	}
	/**
	 * Notify all observers
	 */
	public void notifyFortification()
	{
		for (Observer observer : this.observers) 
		{
           observer.FortificationUpdate(this.fortificationMsg);
        }
	}
	/**
	 * This method is used to get reinforcement message
	 * @return reinforcement message
	 */
	public String getreinforcementMsg()
	{
		return reinforcementMsg ;
	}
	/**
	 * This method is used to set reinforcement message
	 * @param reinforcementMsg:reinforcement message
	 */
	public void setreinforcementMsg(String reinforcementMsg) 
	{
		this.reinforcementMsg = reinforcementMsg;
		notifyReinforcement();
	}
	/**
	 * Notify all observers
	 */
	public void notifyReinforcement()
	{
		for (Observer observer : this.observers) 
		{
           observer.ReinforcementUpdate(this.reinforcementMsg);
        }
	}
	/**
	 *  This method is used to get attack message
	 *  @return attack message
	 */
	public String getattackMsg()
	{
		return attackMsg ;
	}
	/**
	 * This method is used to set attack message
	 * @param attackMsg:attack message
	 */
	public void setattackMsg(String attackMsg)
	{
		this.attackMsg = attackMsg;
		notifyAttack();
	}
	/**
	 * Notify all observers
	 */
	public void notifyAttack()
	{
		for (Observer observer : this.observers) 
		{
           observer.AttackUpdate(this.attackMsg);
        }
	}
	/**
	 * This method is used to get Player Name
	 * @return player Name
	 */
	public String getPlayerName()
	{
		return playerName;
	}
	/**
	 * This method is used to set Player Name
	 * @param playerName:Name of player
	 */
	public void setPlayerName(String playerName)
	{
		this.playerName = playerName;
		notifyPlayerName();
	}
	/**
	 * Notify all observers
	 */
	public void notifyPlayerName()
	{
		for (Observer observer : this.observers) 
		{
           observer.PlayerUpdate(this.playerName);
        }
	}
}
