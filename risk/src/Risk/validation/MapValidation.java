package Risk.validation;

import Risk.model.Territory;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
/**
 * This class is used to Validate Map.
 */
public class MapValidation {
	/**
	 * Validate the Content of Map.
     * @param continentsData input continent data
     * @param territoriesData input territory data
	 * @return true if map is valid after validation else false
	 */
    public boolean validateMap(String continentsData, String territoriesData){
        System.out.println(continentsData);
        if(StringUtils.isBlank(continentsData)){
            System.out.println("Continents can't be null");
            return false;
        }
        if(StringUtils.isBlank(territoriesData)){
            System.out.println("Continents can't be null");
            return false;
        }
        for(String data:territoriesData.split("\n")){
            String[] territoriesDataArray = data.split(",");
            if(territoriesDataArray.length>1){
                if(territoriesDataArray.length==4){
                    System.out.println("Territories don't have adjacent Territories");
                    return false;
                }
            }
        }

        return true;
    }

    /**
	 * This method is used to check if Graph is Connected or not.
     * @param territory input territory data
	 * @return isMapValid  returns true if graph is connected else false
	 */
    public boolean validateMapIsConnected(Territory territory){
        Map<String, Integer> TerritoryStatusMap =territory.getTerritoryStatusMap();
        Map<String, List<String>> adjacentTerritories=territory.getAdjacentTerritories();
        System.out.println("map in connected function");
        System.out.println(TerritoryStatusMap);
        System.out.println(adjacentTerritories);
        int totalTerritories=TerritoryStatusMap.size();
        Queue<String> queue=new LinkedList<>();
        String firstTerritory=adjacentTerritories.keySet().iterator().next();
        // logic to add in queue
        TerritoryStatusMap.put(firstTerritory,1);
        System.out.println(TerritoryStatusMap);
        List<String> adjacent=adjacentTerritories.get(firstTerritory);
        System.out.println(adjacent);
        for(int i=0; i<adjacent.size(); i++){
            String adjacentData= adjacent.get(i);
            queue.add(adjacentData);
        }
        while(!queue.isEmpty()){
            String firstElement= ((LinkedList<String>) queue).pollFirst();
            System.out.println(firstElement);
            List<String> adjacentData=adjacentTerritories.get(firstElement);
            System.out.println(adjacentData);
            for(int i=0; i<adjacentData.size(); i++){
                String adjacentElements= adjacentData.get(i);
                if(!TerritoryStatusMap.get(adjacentElements).equals(1)) {
                    queue.add(adjacentElements);
                    System.out.println(queue);
                }
            }

            System.out.println(queue);
            TerritoryStatusMap.put(firstElement,1);
            System.out.println(TerritoryStatusMap);
        }
        if(TerritoryStatusMap.containsValue(0)){
            System.out.println("disconnected map");
            return false;
        }

        System.out.println(queue);
        return true;
    }
}
