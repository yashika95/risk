package Risk.controller;

import Risk.exception.ExceptionClass;
import Risk.model.Continent;
import Risk.model.Territory;
import Risk.validation.MapValidation;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
/**
 * This class is used to parse and load the map
 */
public class RiskMap {
     Continent continent;
     Territory territory;
    MapValidation mapValidation;
    ExceptionClass exceptionClass = new ExceptionClass();
/**
 * This method is used to parse the map file based on data provided by the user
 * @param continent:Continent selected by the user
 * @param territory:Territory selected by the user
 * @return returns whether map data has been connected correctly or not
 */
    public String readRiskMap(Continent continent, Territory territory) {
        try {

            String filedata = "";
            this.continent = continent;
            this.territory = territory;
            JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            jfc.setDialogTitle("Select Map");
            jfc.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Map", "map");
            jfc.addChoosableFileFilter(filter);
            int returnValue = jfc.showOpenDialog(null);

            if (returnValue == JFileChooser.APPROVE_OPTION) {
                System.out.println(jfc.getSelectedFile().getPath());

                Scanner scanner = new Scanner(new File(jfc.getSelectedFile().getPath()));
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    System.out.println(line);
                    filedata = filedata + System.lineSeparator() + line;
                }

                String continentsData = StringUtils.substringBetween(filedata, "[Continents]", "[Territories]");
                continentsData.trim();
                System.out.println(continentsData);
                String territoriesData = StringUtils.substringAfter(filedata, "[Territories]");
                System.out.println(territoriesData);
                boolean mapValidationResult = mapValidation(continentsData, territoriesData);
                System.out.println(mapValidationResult);
                if (mapValidationResult == false) {
                    return "Invalid Map format";
                } else {
                    setContinetData(continentsData, territoriesData);
                    setTerritoryData(territoriesData);
                    boolean checkMapIsConnectedResult = checkMapisConnected();
                    if (checkMapIsConnectedResult) {
                        return "Map Data connected check successful";
                    } else {
                        return "Diconnected map-Invalid format";
                    }
                }
            }

        } catch (FileNotFoundException e) {

            exceptionClass.exceptionCall(e);

        }
        return null;

    }
/**
 * This function is used to set continent and territory data to continent name
 * @param continentData:Continents selected by the user
 * @param territoryData:Territories selected by the user
 */
    public void setContinetData(String continentData, String territoryData){

        continent.setContinentName(continentData);
        continent.setContinentTerritories(territoryData);
    }
    /**
     * This function is used to set adjecent territories
     * @param territoryData:Territories selected by the user
     */
    public void setTerritoryData(String territoryData){

        territory.setAdjacentTerritories(territoryData);
    }
/**
 * This function is used to validate the map
 * @param continentsData: Data about continents to perform validation
 * @param territoriesData: Data about territories to perform validation
 * @return true or false based on map validation
 */
    public boolean mapValidation(String continentsData, String territoriesData){
        mapValidation=new MapValidation();
        boolean validateMapResult=mapValidation.validateMap(continentsData, territoriesData);
        return validateMapResult;
    }

/**
 * This function is used to check wheather map is connected or not
 * @return true or false based on map connectivity
 */
    public boolean checkMapisConnected(){
        mapValidation=new MapValidation();
        boolean validateMapIsConnected=mapValidation.validateMapIsConnected(territory);
        return validateMapIsConnected;
    }
}