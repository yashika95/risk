package Risk.controller;

import Risk.model.*;
import Risk.observer.DenominationSub;
import Risk.observer.ExchangeSubject;
import Risk.observer.Subject;
import Risk.view.CardExchangeView;
import Risk.view.DominationView;
import Risk.view.PhaseView;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class provide logics to play the game 
 */
public class RiskGame {
    Territory territory;
    RiskMap riskMap;
    Players players;
    Continent continent;
    ArmiesCountCalculation armiesCountCalculation;
    Reinforcement reinforcement;
    Subject subject;
    DenominationSub denominationSub;
    ExchangeSubject exchangeSubject;
    PhaseView phaseView;
    DominationView dominationView;
    CardExchangeView cardExchangeView;
/**
 * Constructor of risk game class
 */
    public RiskGame() {

        players = new Players();
        territory = new Territory();
        continent = new Continent();
        riskMap = new RiskMap();
        armiesCountCalculation = new ArmiesCountCalculation();
        reinforcement = new Reinforcement();
        subject=new Subject();
        denominationSub=new DenominationSub();
        exchangeSubject=new ExchangeSubject();
        phaseView= new PhaseView(subject);
        dominationView= new DominationView(denominationSub);
        cardExchangeView=new CardExchangeView(exchangeSubject);


    }

    /**
     * This function is used to read the map
     *
     * @return riskMapResult:returns the map
     * @throws FileNotFoundException If map file not found
     */
    public String setMap() throws FileNotFoundException {

        String riskMapResult;
        riskMapResult = riskMap.readRiskMap(continent, territory);
        System.out.println(riskMapResult);
        return riskMapResult;

    }

    /**
     * This function is used to assign territories Randomly
     */
    public void assignTerritoriesRandomly() {

        Map<String, Integer> TerritoriesList = territory.getTerritoryStatusMap();
        System.out.println(TerritoriesList);
        players.assignRandomTerritories(TerritoriesList, territory, players);
        Map<String, ArrayList<String>> playersOwnedTerritory = players.getPlayersOwnedTerritories();
        System.out.println(playersOwnedTerritory);
    }

    /**
     * This function is used to create player list
     * @param names:names of the player
     */
    public void createPlayerList(ArrayList<String> names) {

        players.addPlayers(names);

    }

    /**
     * This function is used to assign intial army count
     *
     * @param names:names of the player
     * @param armiesCount:count of armies
     */
    public void assignInitialArmyCount(ArrayList<String> names, Integer armiesCount) {

        for (int i = 0; i < names.size(); i++) {

            players.initialarmyCount(names.get(i), armiesCount);
            System.out.println(players.getinitialarmyCount(names.get(i)));

        }

    }

    /**
     * This function is used to get initial army count
     * @param armiesCountCalculation:provides army count calculation
     * @return armies:army count
     */
    public int initialArmycount (ArmiesCountCalculation armiesCountCalculation) {
        return armiesCountCalculation.getArmiesCount();
    }

    /**
     * This function is used to set reinforcement armies during reinforcement phase
     */
    public void setReinforcementArmies() {
        Map<String, ArrayList<String>> playersOwnedTerritory = players.getPlayersOwnedTerritories();
        reinforcement.setPlayersReinforcementarmyCount(playersOwnedTerritory);

    }

    /**
     * This function is used to set initial army count to the territories
     * @param initialArmiesCount:provide initial army count
     */
    public void assignInitialArmiesToTerritory(int initialArmiesCount) {
        System.out.println(initialArmiesCount);
        players.assignInitialArmytoTerritory(initialArmiesCount);

    }

    /**
     * This function is used to start the game
     */
    public void StartGame() {
        subject.attach(phaseView);
        denominationSub.attach(dominationView);
        exchangeSubject.attach(cardExchangeView);
        while(true){
            boolean endgame=players.StartGame(reinforcement, players, territory, continent, subject,denominationSub,exchangeSubject);
            if(endgame)
                break;
        }
    }

/**
 * This method is used to set card to the territory
 */

    public void setCardsToTerritory()
    {
        territory.setTerritoryCard();
    }
}
