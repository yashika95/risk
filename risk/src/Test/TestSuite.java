
package Test;
import org.junit.runner.RunWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
@RunWith(JUnitPlatform.class)
@SelectPackages({"RiskTest.Model","RiskTest.controller","RiskTest.Validator"})
/**
 * Test suite class.
 */
public class TestSuite 
{

}

