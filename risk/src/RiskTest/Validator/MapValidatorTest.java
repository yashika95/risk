package RiskTest.Validator;
import Risk.controller.RiskMap;
import Risk.model.Continent;
import Risk.model.Territory;
import Risk.validation.MapValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * MapValidator Test class
 *
 */
public class MapValidatorTest
{
    MapValidation mapValidation= new MapValidation();
	String continentData="";
	String territoryData="";
	Continent continent;
	Territory territory;
	public Map<String, List<String>> adjacentTerritories;
	Map<String, Integer> TerritoryStatusMap;

	RiskMap riskMap= new RiskMap();
	/**
	 * This method is used to initialize all Map Data and
	 * invoked at the start of all the test methods.
	 */
	@BeforeEach
	public void beforeTest(){
		territory=new Territory();
		adjacentTerritories=new HashMap<>();
		TerritoryStatusMap=new HashMap<>();

	List<String> adjacentList = new ArrayList<>();
		adjacentList.add("Libya");
		adjacentTerritories.put("India",adjacentList);
		TerritoryStatusMap.put("India",0);

	List<String> adjacentList1 = new ArrayList<>();
		adjacentList1.add("Benin");
		adjacentTerritories.put("Egypt",adjacentList1);
		TerritoryStatusMap.put("Egypt",0);

	List<String> adjacentList2 = new ArrayList<>();
		adjacentList2.add("Benin");
		adjacentTerritories.put("Chad",adjacentList2);
		TerritoryStatusMap.put("Chad",0);

	List<String> adjacentList3 = new ArrayList<>();
		adjacentList3.add("Benin");
		adjacentTerritories.put("Libya",adjacentList3);
		TerritoryStatusMap.put("Libya",0);

	List<String> adjacentList4 = new ArrayList<>();
		adjacentList4.add("Libya");
		adjacentTerritories.put("Benin",adjacentList4);
		TerritoryStatusMap.put("Benin",0);

	List<String> adjacentList5 = new ArrayList<>();
		adjacentList5.add("Benin");
		adjacentTerritories.put("Israel",adjacentList5);
		TerritoryStatusMap.put("Israel",0);

	territory.adjacentTerritories=adjacentTerritories;
	territory.TerritoryStatusMap=TerritoryStatusMap;


	}


	/**
	 * This method is used to test
	 */
	@Test
	public void testValidateMap()
	{

		assertEquals(false,mapValidation.validateMap(continentData, territoryData));
	}
	/**
	 * This method is used to test
	 * @throws FileNotFoundException If file not found
	 */
	@Test
	public void testValidateMap1() throws FileNotFoundException {

		assertEquals("Invalid Map format",riskMap.readRiskMap(continent, territory));
	}

	/**
	 * This method is used to test
	 */
	@Test
	public void testValidateMap2(){

		assertEquals(false, mapValidation.validateMapIsConnected(territory));
	}

}
