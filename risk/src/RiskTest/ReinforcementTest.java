package RiskTest;

import Risk.model.Reinforcement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Reinforcement Test Class
 */

public class ReinforcementTest 
{
	HashMap<String, Integer> playersReinforcementarmyCount;

	Reinforcement reinforcement;




	/**
	 * This method is invoked at the start of all the test methods.
	 */
	@BeforeEach
	public void beforeTest() {
		reinforcement = new Reinforcement();



	}

	@Test
	public void testGenerateArmy()
	{   reinforcement.setPlayerReinforcementArmyCount("Player 1", 35);
		assertEquals(35,reinforcement.getPlayerReinforcementArmyCount("Player 1"));
	}



}
