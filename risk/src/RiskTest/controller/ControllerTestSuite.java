package RiskTest.controller;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;



@RunWith(JUnitPlatform.class)
@SelectPackages("RiskTest.controller")
/**
 * Test suite class.
 */
public class ControllerTestSuite 
{

}
