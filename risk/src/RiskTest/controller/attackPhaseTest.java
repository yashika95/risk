package RiskTest.controller;
import Risk.model.Players;
import Risk.model.Territory;
import Risk.observer.Subject;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.Test;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 * Test class for attack phase
 */
public class attackPhaseTest 
{
	Players players;
	Territory territory;
	Subject subject;
	public HashMap<String, ArrayList<Integer>> assignedArmies;
	public Map<String, ArrayList<String>> playersOwnedTerritories;
	public ArrayList<String> playersNameList;
	/**
	 * This method is invoked at the start of all the test methods.
	 */
	@BeforeEach public void beforeTest() 
	{
		players=new Players();
		territory=new Territory();
		subject=new Subject();
		assignedArmies=new HashMap<>();
		playersOwnedTerritories=new HashMap<>();
		playersNameList=new ArrayList<>();
		ArrayList<Integer> list =new ArrayList<>();
		list.add(2);
		list.add(3);
		assignedArmies.put("a", list);

		ArrayList<Integer> list1 =new ArrayList<>();
		list1.add(1);
		list1.add(2);
		assignedArmies.put("b", list1);

		ArrayList<Integer> list2 =new ArrayList<>();
		list2.add(1);
		list2.add(1);
		assignedArmies.put("c", list2);
		players.assignedArmies=assignedArmies;

		ArrayList<String> territoryList =new ArrayList<>();
		territoryList.add("India");
		territoryList.add("Libya");
		playersOwnedTerritories.put("a", territoryList);

		ArrayList<String> territoryList1 =new ArrayList<>();
		territoryList.add("Benin");
		territoryList.add("Chad");
		playersOwnedTerritories.put("b", territoryList1);

		ArrayList<String> territoryList2 =new ArrayList<>();
		territoryList.add("Egypt");
		territoryList.add("Isael");
		playersOwnedTerritories.put("c", territoryList2);

		players.playersOwnedTerritories=playersOwnedTerritories;
		playersNameList.add("a");
		playersNameList.add("b");
		playersNameList.add("c");
		players.playersNameList=playersNameList;



	}
	/**
	 * Test1 for attack phase
	 */
	@Test public void attackPhaseTest1()
	{   assertEquals(true, players.checkIsAttackPossible("b"));
	}
	/**
	 * Test2 for attack phase
	 */
	@Test public void attackPhaseTest2()
	{
		assertEquals(true, players.checkIsAttackPossible("a"));
	}
	/**
	 * Test3 for attack phase
	 */
	@Test public void attackPhaseTest3()
	{
		assertEquals(false, players.checkIsAttackPossible("c"));
	}
	/**
	 * Test4 for attack phase
	 */
	@Test public void attackPhaseTest4()
	{
		assertEquals(true,players.setArmyToAttackedTerritory("a","India","Libya",1));
	}
	/**
	 * Test5 for attack phase
	 */
	@Test public void attackPhaseTest5()
	{
		assertEquals(false,players.setArmyToAttackedTerritory("b","Egypt","Israel",1));

	}

	/**
	 * Test6 for attack phase
	 */
	@Test public void attackPhaseTest6()
	{
		assertEquals(false,players.doAllOutAttack("b","Israel","India", players, subject));

	}

	/**
	 * Test6 for attack phase
	 */
	@Test public void attackPhaseEndTest()
	{
		assertEquals(false,players.doAllOutAttack("b","Israel","India", players, subject));

	}
}
