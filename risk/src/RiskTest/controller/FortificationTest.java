package RiskTest.controller;
import Risk.model.Players;
import Risk.model.Reinforcement;
import Risk.model.Territory;
import Risk.observer.Subject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

//import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 * Test class for Fortification phase
 */
public class FortificationTest 
{
	Players players;
	Reinforcement reinforcement;
	Territory territory;
	Subject subject;
	public HashMap<String, ArrayList<Integer>> assignedArmies;
	public Map<String, ArrayList<String>> playersOwnedTerritories;
	public ArrayList<String> playersNameList;
	Map<String, List<String>> adjacentTerritories;

	/**
	 * This method is invoked at the start of all the test methods.
	 */
	@BeforeEach public void beforeTest() 
	{
		players=new Players();
		reinforcement=new Reinforcement();
		territory=new Territory();
		subject=new Subject();

		assignedArmies=new HashMap<>();
		playersOwnedTerritories=new HashMap<>();
		playersNameList=new ArrayList<>();
		adjacentTerritories= new HashMap<>();
		ArrayList<Integer> list =new ArrayList<>();
		list.add(2);
		list.add(3);
		assignedArmies.put("a", list);

		ArrayList<Integer> list1 =new ArrayList<>();
		list1.add(1);
		list1.add(2);
		assignedArmies.put("b", list1);

		ArrayList<Integer> list2 =new ArrayList<>();
		list2.add(1);
		list2.add(1);
		assignedArmies.put("c", list2);
		players.assignedArmies=assignedArmies;

		ArrayList<String> territoryList =new ArrayList<>();
		territoryList.add("India");
		territoryList.add("Libya");
		playersOwnedTerritories.put("a", territoryList);

		ArrayList<String> territoryList1 =new ArrayList<>();
		territoryList.add("Benin");
		territoryList.add("Chad");
		playersOwnedTerritories.put("b", territoryList1);

		ArrayList<String> territoryList2 =new ArrayList<>();
		territoryList.add("Egypt");
		territoryList.add("Israel");
		playersOwnedTerritories.put("c", territoryList2);

		players.playersOwnedTerritories=playersOwnedTerritories;
		playersNameList.add("a");
		playersNameList.add("b");
		playersNameList.add("c");
		players.playersNameList=playersNameList;
		List<String> adjacentList = new ArrayList<>();
		adjacentList.add("Libya");
		adjacentTerritories.put("India",adjacentList);

		List<String> adjacentList1 = new ArrayList<>();
		adjacentList1.add("Benin");
		adjacentTerritories.put("Egypt",adjacentList1);

		List<String> adjacentList2 = new ArrayList<>();
		adjacentList2.add("Benin");
		adjacentTerritories.put("Chad",adjacentList2);

		List<String> adjacentList3 = new ArrayList<>();
		adjacentList3.add("Benin");
		adjacentTerritories.put("Libya",adjacentList3);

		List<String> adjacentList4 = new ArrayList<>();
		adjacentList4.add("Libya");
		adjacentTerritories.put("Benin",adjacentList4);

		List<String> adjacentList5 = new ArrayList<>();
		adjacentList5.add("Benin");
		adjacentTerritories.put("Israel",adjacentList5);

       territory.adjacentTerritories=adjacentTerritories;


	}
	/**
	 * Test1 for Fortification phase
	 */
	@Test public void FortificationTest1()
	{
		assertEquals(false,players.fortificationPhase(2,"abc",players, reinforcement, territory,subject) );
	}
	/**
	 * Test2 for Fortification phase
	 */
	@Test public void FortificationTest2()
	{
		assertEquals(true, players.checkConnectedAdjacent("India","Libya", "a", territory));
	}
	/**
	 * Test3 for Fortification phase
	 */
	@Test public void FortificationTest3()
	{
		assertEquals(true, players.checkConnectedAdjacent("Chad","Libya", "a", territory));

	}
	/**
	 * Test4 for Fortification phase
	 */
	@Test public void FortificationTest4()
	{
		assertEquals(false, players.checkConnectedAdjacent("India","Israel", "a", territory));

	}
	/**
	 * Test5 for Fortification phase
	 */
	@Test public void FortificationTest5()
	{
		assertEquals(false, players.checkConnectedAdjacent("Chad","Egypt", "a", territory));

	}
	/**
	 * Test6 for Fortification phase
	 */
	@Test public void FortificationTest6()
	{
		assertEquals(true, players.checkConnectedAdjacent("Benin","Libya", "a", territory));

	}
	/**
	 * Test7 for Fortification phase
	 */
	@Test public void FortificationTest7()
	{
		assertEquals(false, players.checkConnectedAdjacent("Benin","India", "a", territory));

	}
}
