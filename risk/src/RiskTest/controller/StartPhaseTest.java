package RiskTest.controller;
import Risk.controller.RiskGame;
import Risk.model.ArmiesCountCalculation;
import Risk.model.Players;
import Risk.model.Territory;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
/**
 * Test class for StartUp phase
 */
public class StartPhaseTest
{
	RiskGame riskGame;
	Players players;
	ArmiesCountCalculation armiesCountCalculation;
	Territory territory;
	ArrayList<String> playerNames;
	public Map<String, ArrayList<String>> playersTerritories;
	ArrayList<String> territories;
	int numberofPlayers;
	int initialArmyCount = 25;
	Map<String, Integer> count;
	int armyCount;

	/**
	 * This method is invoked before the start of all the test methods.
	 */
	@BeforeEach public void beforeTest() 
	{
		riskGame = new RiskGame();
		players = new Players();
		armiesCountCalculation = new ArmiesCountCalculation();
		territory = new Territory();
		numberofPlayers =5;
		playerNames = new ArrayList<>();

		playerNames.add("Tushar");
		playerNames.add("Yashika");
		playerNames.add("Shivam");
		players.playersNameList = playerNames;
		armiesCountCalculation.initialArmiesCountCalculation(numberofPlayers);
		count = players.initialarmyCount("Tushar", armiesCountCalculation.getArmiesCount());
		armyCount = count.get("Tushar");

	}
	/**
	 * Test1 for StartUp Phase
	 */
	@Test public void StartPhaseTest1()
	{

		assertEquals(playerNames , players.getPlayersNameList());
		
	}
	/**
	 * Test2 for StartUp Phase
	 */
	@Test public void StartPhaseTest2()
	{
		assertEquals(initialArmyCount, armiesCountCalculation.getArmiesCount());
	}
	/**
	 * Test3 for StartUp Phase
	 */
	@Test public void StartPhaseTest3()
	{
		assertNotEquals(23, players.getinitialarmyCount("Tushar"));
	}

}
