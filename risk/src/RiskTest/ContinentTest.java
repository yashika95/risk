package RiskTest;
import Risk.model.Continent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ContinentTest
{
	Continent continent;
	String continentNameOne = "Continent one";
	String continentNameTwo = "Continent two";


	int controlValueOne = 4;
	int controlValueTwo = 2;

	Map<String, Integer> continentName;
	Map<String, Integer> continentName2;


	/**
	 * This method is invoked at the start of all the test methods.
	 */
	@BeforeEach
	public void beforeTest() {
		continent = new Continent();
		continentName = new HashMap<>();
		continentName.put(continentNameOne, controlValueOne);
		continentName2 = new HashMap<>();
		continentName2.put(continentNameTwo, controlValueTwo);

	}


	/**
	 * This method is to test set continent and it's control value functionality.
	 */
	@Test
	public void testSetContinentValue() 
	{   continent.setContinentName("Continent one=4");
		assertEquals(continentName,continent.getContinenttName());
	}

	/**
	 * This method is to test set continent and it's control value functionality.
	 */
	@Test
	public void testSetContinentValue2()
	{   continent.setContinentName("Continent two=2");
		assertEquals(continentName2,continent.getContinenttName());
	}
	/**
	 * This method is to test set continent and it's control value functionality.
	 */
	@Test
	public void testSetContinentValue3()
	{
		continent.setContinentName("Continent two=3");
		assertNotEquals(continentName2,continent.getContinenttName());
	}
	/**
	 * This method is to test set continent and it's control value functionality.
	 */
	@Test
	public void testSetContinentValue4()
	{
		continent.setContinentName("Continent two=4");
		assertNotEquals(continentName2,continent.getContinenttName());
	}
	/**
	 * This method is to test set continent and it's control value functionality.
	 */
	@Test
	public void testSetContinentValue5()
	{
		continent.setContinentName("Continent two=6");
		assertNotEquals(continentName2,continent.getContinenttName());
	}
}
