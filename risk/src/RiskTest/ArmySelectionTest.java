package RiskTest;
import Risk.model.ArmiesCountCalculation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArmySelectionTest
{
	int twoPlayerArmy = 40;
	int threePlayerArmy = 35;
	int fourPlayerArmy = 30;
	int fivePlayerArmy = 25;

	ArmiesCountCalculation twoPlayer=new ArmiesCountCalculation();
	ArmiesCountCalculation threePlayer=new ArmiesCountCalculation();
	ArmiesCountCalculation fourPlayer=new ArmiesCountCalculation();
	ArmiesCountCalculation fivePlayer=new ArmiesCountCalculation();


	
	/**
	 * This method is invoked at the start of all the test methods.
	 */
	@BeforeEach public void beforeTest() 
	{
		twoPlayer.initialArmiesCountCalculation(2);
		threePlayer.initialArmiesCountCalculation(3);
		fourPlayer.initialArmiesCountCalculation(4);
		fivePlayer.initialArmiesCountCalculation(5);
	}
	@Test public void testArmiesSelection() 
	{
		assertEquals(twoPlayerArmy,twoPlayer.getArmiesCount());

	}
	@Test public void testArmiesSelection1()
	{
		assertEquals(threePlayerArmy,threePlayer.getArmiesCount());

	}
	@Test public void testArmiesSelection2()
	{
		assertEquals(fourPlayerArmy,fourPlayer.getArmiesCount());

	}
	@Test public void testArmiesSelection3()
	{
		assertEquals(fivePlayerArmy,fivePlayer.getArmiesCount());
	}
}
