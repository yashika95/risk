package RiskTest;
/**
 * MapValidator Test class
 *
 */


import Risk.controller.RiskMap;
import Risk.model.Continent;
import Risk.model.Territory;
import Risk.validation.MapValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class MapValidatorTest
{
    MapValidation mapValidation= new MapValidation();
	String continentData="";
	String territoryData="";
	Continent continent;
	Territory territory;

	RiskMap riskMap= new RiskMap();
	/**
	 * This method is used to initialize all Map Data and
	 * invoked at the start of all the test methods.
	 */
	@BeforeEach
	public void beforeTest() 
	{

	}


	/**
	 * This method is used to test
	 */
	@Test
	public void testValidateMap()
	{

		assertEquals(true,mapValidation.validateMap(continentData, territoryData));
	}

	@Test
	public void testValidateMap1() throws FileNotFoundException {

		assertEquals("Invalid Map format",riskMap.readRiskMap(continent, territory));
	}


}
