package RiskTest.Model;

import Risk.model.Players;
import Risk.model.Territory;
import Risk.observer.ExchangeSubject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CardsTest {
    Players players;
    public Map<String, ArrayList<String>> playersOwnedCards;
    Territory territory;
    ExchangeSubject exchangeSubject;
    /**
     * This method is invoked at the start of all the test methods.
     */
    @BeforeEach
    public void beforeTest()
    {
        players=new Players();
        exchangeSubject=new ExchangeSubject();
        territory=new Territory();
        playersOwnedCards=new HashMap<>();
        ArrayList<String> list=new ArrayList<>();
        playersOwnedCards.put("a",list);
        players.playersOwnedCards=playersOwnedCards;
    }
    /**
     * Test1 for StartUp Phase
     */
    @Test
    public void CardTest1()
    {
        assertEquals(0, players.cardView("a",players, territory,exchangeSubject));
    }
    /**
     * Test2 for StartUp Phase
     */
    @Test public void CardTest2()
    {
        assertEquals(true, players.checkCardValid("Benin-infantry","India-cavalry","Egypt-artillery"));
    }
    /**
     * Test3 for StartUp Phase
     */
    @ Test public void CardTest3()
    {
        assertEquals(false, players.checkCardValid("Benin-infantry","Benin-infantry","Egypt-artillery"));

    }
    /**
     * Test3 for StartUp Phase
     */
    @ Test public void CardTest4()
    {
        assertEquals(true, players.checkCardValid("Benin-infantry","Benin-infantry","Benin-infantry"));

    }
    /**
     * Test3 for StartUp Phase
     */
    @ Test public void CardTest5()
    {
        assertEquals(false, players.checkCardValid("Benin-infantry","Egypt-artillery","Egypt-artillery"));

    }
    /**
     * Test3 for StartUp Phase
     */
    @ Test public void CardTest6()
    {
        assertEquals(true, players.checkCardValid("India-cavalry","Benin-infantry","Egypt-artillery"));

    }
    /**
     * Test3 for StartUp Phase
     */
    @ Test public void CardTest7()
    {
        assertEquals(true, players.checkCardValid("India-cavalry","India-cavalry","India-cavalry"));

    }
    /**
     * Test3 for StartUp Phase
     */
    @ Test public void CardTest8()
    {
        assertEquals(true, players.checkCardValid("Egypt-artillery","Benin-infantry","India-cavalry"));

    }
    /**
     * Test3 for StartUp Phase
     */
    @ Test public void CardTest9()
    {
        assertEquals(false, players.checkCardValid("Egypt-artillery","India-cavalry","India-cavalry"));

    }
    /**
     * Test3 for StartUp Phase
     */
    @ Test public void CardTest10()
    {
        assertEquals(false, players.checkCardValid("India-infantry","Benin-infantry","Egypt-artillery"));

    }
}
