package RiskTest.Model;

import Risk.model.Continent;
import Risk.model.Players;
import Risk.model.Reinforcement;
import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Reinforcement Test Class
 */

public class ReinforcementTest 
{
	HashMap<String, Integer> playersReinforcementarmyCount;

	Reinforcement reinforcement;
	public HashMap<String, ArrayList<Integer>> assignedArmies;
	public Map<String, ArrayList<String>> playersOwnedTerritories;
	public ArrayList<String> playersNameList;
	Players players;
	public Map<String, List<String>> continentTerritories;
	Continent continent;
	public Map<String, Integer> continentName;




	/**
	 * This method is invoked at the start of all the test methods.
	 */
	@BeforeEach
	public void beforeTest() {
		continentTerritories=new HashMap<>();
		continentName=new HashMap<>();
		players=new Players();
		continent=new Continent();
		reinforcement = new Reinforcement();
		assignedArmies=new HashMap<>();
		playersOwnedTerritories=new HashMap<>();
		playersNameList=new ArrayList<>();
		ArrayList<Integer> list =new ArrayList<>();
		list.add(2);
		list.add(3);
		assignedArmies.put("a", list);

		ArrayList<Integer> list1 =new ArrayList<>();
		list1.add(1);
		list1.add(2);
		assignedArmies.put("b", list1);

		ArrayList<Integer> list2 =new ArrayList<>();
		list2.add(1);
		list2.add(1);
		assignedArmies.put("c", list2);
		players.assignedArmies=assignedArmies;

		ArrayList<String> territoryList =new ArrayList<>();
		territoryList.add("India");
		territoryList.add("Libya");
		territoryList.add("Benin");
		territoryList.add("Egypt");
		playersOwnedTerritories.put("a", territoryList);

		ArrayList<String> territoryList1 =new ArrayList<>();
		territoryList.add("Benin");
		territoryList.add("Chad");
		playersOwnedTerritories.put("b", territoryList1);

		ArrayList<String> territoryList2 =new ArrayList<>();
		territoryList.add("Egypt");
		territoryList.add("Isael");
		playersOwnedTerritories.put("c", territoryList2);

		players.playersOwnedTerritories=playersOwnedTerritories;
		playersNameList.add("a");
		playersNameList.add("b");
		playersNameList.add("c");
		players.playersNameList=playersNameList;

		List<String> continentTerritorylist = new ArrayList<String>();
		continentTerritorylist.add("India");
		continentTerritorylist.add("Libya");
		continentTerritorylist.add("Benin");
		continentTerritories.put("Asia",continentTerritorylist);

		continent.continentTerritories=continentTerritories;

		continentName.put("Asia",4);
		continent.continentName=continentName;

	}
/**
 * Test for Generation of armies
 */
	@Test
	public void testGenerateArmy()
	{   reinforcement.setPlayerReinforcementArmyCount("Player 1", 35);
		assertEquals(35,reinforcement.getPlayerReinforcementArmyCount("Player 1"));
	}

	@Test
	public void test()
	{
		assertEquals(2,players.getTerritoryArmy("India"));
	}

	@Test
	public void test2()
	{
		assertEquals(4,players.playerOwnedContinentsArmy("a",continent,players));
	}

	@Test
	public void test3()
	{
		assertEquals(0,players.playerOwnedContinentsArmy("b",continent,players));
	}

	@Test
	public void test4()
	{
		assertEquals(0,players.playerOwnedContinentsArmy("c",continent,players));
	}
}
